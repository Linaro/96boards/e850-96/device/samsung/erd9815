#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/aosp_erd9815.mk \
    $(LOCAL_DIR)/full_smdk9815_r.mk \
    $(LOCAL_DIR)/full_erd9815_r.mk \
    $(LOCAL_DIR)/full_erd9813_r.mk \
    $(LOCAL_DIR)/full_erd9815P_r.mk \
    $(LOCAL_DIR)/full_erd9811_r.mk \
    $(LOCAL_DIR)/full_erd9815_rt.mk

COMMON_LUNCH_CHOICES := \
    full_smdk9815_r-eng \
    full_smdk9815_r-userdebug \
    full_smdk9815_r-user \
    full_erd9815_r-eng \
    full_erd9815_r-userdebug \
    full_erd9815_r-user \
    full_erd9815_rt-eng \
    full_erd9815_rt-userdebug \
    full_erd9815_rt-user \
    full_erd9813_r-eng \
    full_erd9813_r-userdebug \
    full_erd9813_r-user \
    full_erd9815P_r-eng \
    full_erd9815P_r-userdebug \
    full_erd9815P_r-user \
    full_erd9811_r-eng \
    full_erd9811_r-userdebug \
    full_erd9811_r-user \
