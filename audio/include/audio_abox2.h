/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __EXYNOS_AUDIOPROXY2_ABOX_H__
#define __EXYNOS_AUDIOPROXY2_ABOX_H__

/* A-Box HW limitations */
#define ABOX_DEV           "/devices/platform/16850000.abox"
#define ABOX_SYS_DEV       "/sys/devices/platform/16850000.abox"
#define ABOX_REGMAP_PATH   "/sys/kernel/debug/regmap/16850000.abox/"

/* SoC Specific scenario support definition */
#define ABOX_SUPPORT_SPKAMP_PROTECTION          false
#define ABOX_SUPPORT_MUTE_STREAM_FOR_SPK        true
#define ABOX_SUPPORT_HIFI_CODEC                 false
#define ABOX_SUPPORT_USB_OFFLOAD                true
#define ABOX_ENABLE_ROUTING_ACTIVE_NOTI         true

/* A-Box Interface Type */
enum {
    UAIF0 = 0,
    UAIF1,
    UAIF2,
    UAIF3,
    UAIF4,
};

enum {
    CODEC = UAIF3,
    HIFI_CODEC = UAIF1,
    SPK = UAIF0,
    BT = UAIF4,
};

// Mixer controls for USB playback loop WDMA configuration
#define MIXER_CTL_ABOX_USB_OUT_LOOP_SAMPLERATE          "ABOX WDMA3 Rate"
#define MIXER_CTL_ABOX_USB_OUT_LOOP_WIDTH               "ABOX WDMA3 Width"
#define MIXER_CTL_ABOX_USB_OUT_LOOP_CHANNEL             "ABOX WDMA3 Channel"
#define MIXER_CTL_ABOX_USB_OUT_LOOP_PERIOD_SZ           "ABOX WDMA3 Period"
#define MIXER_CTL_ABOX_USB_OUT_LOOP_ASRC                "ABOX SPUM ASRC3"

// Mixer control for USB capture loop RDMA configuration
#define MIXER_CTL_ABOX_USB_IN_LOOP_SAMPLERATE           "ABOX RDMA10 Rate"
#define MIXER_CTL_ABOX_USB_IN_LOOP_WIDTH                "ABOX RDMA10 Width"
#define MIXER_CTL_ABOX_USB_IN_LOOP_CHANNEL              "ABOX RDMA10 Channel"
#define MIXER_CTL_ABOX_USB_IN_LOOP_PERIOD_SZ            "ABOX RDMA10 Period"

#define MIXER_CTL_ABOX_BT_UAIF_SAMPLERATE               "ABOX UAIF4 Rate"
// BTA2DP offload loopback RDMA path controls
#define MIXER_CTL_ABOX_BTA2DP_RDMA_LOOP_INPUT           "ABOX RDMA6_A"
#define MIXER_SET_ABOX_BTA2DP_COMP_OUTPUT               "A2DPCOM"

#define MIXER_CTL_SMARTPA_MUTE                          "SmartPA Mute"

/* Audiologging A-box PCM dump point for this project */
#define PCM_DUMP_CNT   15
#define ABOX_NODE_NAME_SZ 5
const char supported_dump[PCM_DUMP_CNT][ABOX_NODE_NAME_SZ] = {"rd1", "rd3", "rd4", "rd6", "rd7", "rdb",
                                                              "wr0", "wr1", "wr2", "wr3", "wr4",
                                                              "uou", "uin", "cs0", "cs1"};

#endif  // __EXYNOS_AUDIOPROXY_ABOX_H__
