/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __EXYNOS_AUDIOPROXY_PCM_H__
#define __EXYNOS_AUDIOPROXY_PCM_H__

#include <tinyalsa/asoundlib.h>
#include <tinycompress/tinycompress.h>
#include <compress_params.h>

/* Actual HW DMA mapped Sound Card & Device Definition */
#define SOUND_CARD0                     0

// Sound Devices mapped for A-Box RDMA
#define SOUND_DEVICE_ABOX_RDMA0         0       // A-Box RDMA0
#define SOUND_DEVICE_ABOX_RDMA1         1       // A-Box RDMA1
#define SOUND_DEVICE_ABOX_RDMA2         2       // A-Box RDMA2
#define SOUND_DEVICE_ABOX_RDMA3         3       // A-Box RDMA3
#define SOUND_DEVICE_ABOX_RDMA4         4       // A-Box RDMA4
#define SOUND_DEVICE_ABOX_RDMA5         5       // A-Box RDMA5
#define SOUND_DEVICE_ABOX_RDMA6         6       // A-Box RDMA6
#define SOUND_DEVICE_ABOX_RDMA7         7       // A-Box RDMA7
#define SOUND_DEVICE_ABOX_RDMA8         8       // A-Box RDMA8
#define SOUND_DEVICE_ABOX_RDMA9         9       // A-Box RDMA9
#define SOUND_DEVICE_ABOX_RDMA10        10      // A-Box RDMA10
#define SOUND_DEVICE_ABOX_RDMA11        11      // A-Box RDMA11

// Sound Devices mapped for A-Box WDMA
#define SOUND_DEVICE_ABOX_WDMA0         12      // A-Box WDMA0
#define SOUND_DEVICE_ABOX_WDMA1         13      // A-Box WDMA1
#define SOUND_DEVICE_ABOX_WDMA2         14      // A-Box WDMA2
#define SOUND_DEVICE_ABOX_WDMA3         15      // A-Box WDMA3
#define SOUND_DEVICE_ABOX_WDMA4         16      // A-Box WDMA4

#define SOUND_DEVICE_AUX                19      // Aux Digital Device for DP Audio
#define SOUND_DEVICE_VTS_TRIGGER        20      // Voice Trigger Device for VTS
#define SOUND_DEVICE_VTS_RECORD         21      // Voice Record Device for VTS

/* Virtual PCM DAIs with SOUND CARD0 */
// Playback device
#define SOUND_DEVICE_VIRT_PRIMARY_PLAYBACK      100     // primary playback virtual device
#define SOUND_DEVICE_VIRT_FAST_PLAYBACK         101     // Fast playback virtual device
#define SOUND_DEVICE_VIRT_AAUDIO_PLAYBACK       102     // AAudio playback virtual device
#define SOUND_DEVICE_VIRT_DEEP_PLAYBACK         103     // Deep playback virtual device
#define SOUND_DEVICE_VIRT_VRX_PLAYBACK          104     // CP Voice Rx playback virtual device
#define SOUND_DEVICE_VIRT_VOIP_PLAYBACK         105     // VoIP playback virtual device, bt-a2dp trigger
#define SOUND_DEVICE_VIRT_INCALLMUSIC_PLAYBACK  106     // InCall music playback virtual device
#define SOUND_DEVICE_VIRT_ULTRASOUND_PLAYBACK   108     // Ultra sound playback virtual device
#define SOUND_DEVICE_VIRT_MUTE_PLAYBACK         109     // Mute playback virtual device (for BT-A2DP)
#define SOUND_DEVICE_DIRECT_DEEP_PLAYBACK       SOUND_DEVICE_ABOX_RDMA0

// Capture device
#define SOUND_DEVICE_VIRT_PRIMARY_CAPTURE       110     // primary capture virtual device
#define SOUND_DEVICE_VIRT_MMAP_CAPTURE          111     // MMAP capture virtual device
#define SOUND_DEVICE_VIRT_LOWLATENCY_CAPTURE    112     // low-latency capture virtual device
#define SOUND_DEVICE_VIRT_VTX_CAPTURE           113     // Voice call TX virtual device
#define SOUND_DEVICE_CALL_RECORD                114     // Call Recording
#define SOUND_DEVICE_VIRT_BARGEIN_TRIGGER       115     // Barge-In trigger virtual device
#define SOUND_DEVICE_VIRT_BARGEIN_CAPTURE       116     // Barge-In capture or FM Radio capture virtual device
#define SOUND_DEVICE_VIRT_VOIP_CAPTURE          117     // VoIP capture virtual device
#define SOUND_DEVICE_VIRT_ULTRASOUND_CAPTURE    118     // Ultra sound capture virtual device
#define SOUND_DEVICE_VIRT_BARGEIN_DUMMY_CAP     119     // Dummy Capture device for BARGEIN
#define SOUND_DEVICE_VIRT_TRIGGER               120     // Trigger virtual device for SPKAMP


/* Virtual DMA/PCM DAI Sound Card & Device Definition */
#define SOUND_CARD1                     1
#define SOUND_CARD2                     2

#define SOUND_DEVICE_UNDEFINE           99

// Channel count definitions
#define MEDIA_1_CHANNEL                         1
#define MEDIA_4_CHANNELS                        4
#define MEDIA_8_CHANNELS                        8

/* Default values for Media PCM Configuration */
#define DEFAULT_MEDIA_CHANNELS          2                   // Stereo
#define DEFAULT_MEDIA_SAMPLING_RATE     48000               // 48KHz
#define DEFAULT_MEDIA_FORMAT            PCM_FORMAT_S16_LE   // 16bit PCM

// Definition for MMAP Stream
#define MMAP_PERIOD_SIZE ((DEFAULT_MEDIA_SAMPLING_RATE/1000) * 5) // MMAP period size set as 5ms due to HW limitation
#define MMAP_PERIOD_COUNT_MIN 32
#define MMAP_PERIOD_COUNT_MAX 512
#define MMAP_PERIOD_COUNT_DEFAULT (MMAP_PERIOD_COUNT_MAX)

/* Default values for Voice PCM Configuration */
#define SAMPLING_RATE_NB                8000                // 8KHz(Narrow Band)
#define SAMPLING_RATE_WB                16000               // 16KHz(Wide Band)
#define SAMPLING_RATE_SWB               32000               // 32KHz(Wide Band)
#define SAMPLING_RATE_FB                48000               // 48KHz(Full Band)

#define DEFAULT_VOICE_CHANNELS          2                   // Stereo
#define DEFAULT_VOICE_SAMPLING_RATE     SAMPLING_RATE_FB
#define DEFAULT_VOICE_FORMAT            PCM_FORMAT_S16_LE   // 16bit PCM

/* Default values for CP Voice Recording PCM Configuration */
#define DEFAULT_VOICE_REC_CHANNELS      2                   // Stereo
#define DEFAULT_VOICE_REC_SAMPLINGRATE  SAMPLING_RATE_SWB   // 32KHz
#define DEFAULT_VOICE_REC_PERIODSIZE    640                 // Sync with A-Box Firmware
#define DEFAULT_VOICE_REC_PERIODCOUNT   4
#define DEFAULT_VOICE_REC_FORMAT        PCM_FORMAT_S16_LE   // 16bit PCM

/* Default values for FM Recording PCM Configuration */
#define DEFAULT_FM_REC_CHANNELS         2                   // Stereo
#define DEFAULT_FM_REC_SAMPLINGRATE     48000               // 48KHz
#define DEFAULT_FM_REC_PERIODSIZE       960                 // Sync with A-Box Firmware
#define DEFAULT_FM_REC_PERIODCOUNT      2
#define DEFAULT_FM_REC_FORMAT           PCM_FORMAT_S16_LE   // 16bit PCM

#define UHQA_MEDIA_FORMAT               PCM_FORMAT_S24_LE   // 24bit PCM
#define UHQA_MEDIA_SAMPLING_RATE        192000

#define SUHQA_MEDIA_FORMAT              PCM_FORMAT_S32_LE   // 32bit PCM
#define SUHQA_MEDIA_SAMPLING_RATE       384000

/* Default bitwidth */
#define DEFAULT_SIFS0_MEDIA_BITWIDTH    32
#define DEFAULT_MEDIA_16BIT_WIDTH       16

/* USB Playback or Capture supported and unsupported channels */
#define ABOX_UNSUPPORTED_CHANNELS           6
#define ABOX_SUPPORTED_MAX_CHANNELS         8

/* Default values for VTS Record Configuration */
#define VTS_RECORD_CARD                 SOUND_CARD0
#define DEFAULT_VTS_RECORD_CHANNELS     2
#define DEFAULT_VTS_RECORD_SAMPLINGRATE 16000
#define DEFAULT_VTS_RECORD_PERIODSIZE   80
#define DEFAULT_VTS_RECORD_PERIODCOUNT  1024
#define DEFAULT_VTS_RECORD_FORMAT       PCM_FORMAT_S16_LE

//----------------------------------------------------------------------------------------------//
// For Playback (Speaker) Path

/* PCM Configurations */
// PCM Configurations for no-attribute Playback Stream
#define NOATTRIBUTE_PLAYBACK_CARD       SOUND_CARD0
#define NOATTRIBUTE_PLAYBACK_DEVICE     SOUND_DEVICE_ABOX_RDMA0
#define NOATTRIBUTE_PLAYBACK_IS_MMAP    true

// PCM Configurations for Primary Playback Stream
#define PRIMARY_PLAYBACK_CARD           SOUND_CARD0
#define PRIMARY_PLAYBACK_DEVICE         SOUND_DEVICE_VIRT_PRIMARY_PLAYBACK
#define PRIMARY_PLAYBACK_IS_MMAP        false

#define PRIMARY_PLAYBACK_CHANNELS       DEFAULT_MEDIA_CHANNELS
#define PRIMARY_PLAYBACK_SAMPLING_RATE  DEFAULT_MEDIA_SAMPLING_RATE
#define PRIMARY_PLAYBACK_PERIOD_SIZE    960
#define PRIMARY_PLAYBACK_PERIOD_COUNT   4
#define PRIMARY_PLAYBACK_FORMAT         DEFAULT_MEDIA_FORMAT
#define PRIMARY_PLAYBACK_START          PRIMARY_PLAYBACK_PERIOD_SIZE
#define PRIMARY_PLAYBACK_STOP           (PRIMARY_PLAYBACK_PERIOD_SIZE * PRIMARY_PLAYBACK_PERIOD_COUNT)

const struct pcm_config pcm_config_primary_playback = {
    .channels        = PRIMARY_PLAYBACK_CHANNELS,
    .rate            = PRIMARY_PLAYBACK_SAMPLING_RATE,
    .period_size     = PRIMARY_PLAYBACK_PERIOD_SIZE,
    .period_count    = PRIMARY_PLAYBACK_PERIOD_COUNT,
    .format          = PRIMARY_PLAYBACK_FORMAT,
    .start_threshold = PRIMARY_PLAYBACK_START,
    .stop_threshold  = PRIMARY_PLAYBACK_STOP,
};

// PCM Configurations for Fast Playback Stream
#define FAST_PLAYBACK_CARD              SOUND_CARD0
#define FAST_PLAYBACK_DEVICE            SOUND_DEVICE_VIRT_FAST_PLAYBACK
#define FAST_PLAYBACK_IS_MMAP           false

#define FAST_PLAYBACK_CHANNELS          DEFAULT_MEDIA_CHANNELS
#define FAST_PLAYBACK_SAMPLING_RATE     DEFAULT_MEDIA_SAMPLING_RATE
#define FAST_PLAYBACK_PERIOD_SIZE       240
#define FAST_PLAYBACK_PERIOD_COUNT      10
#define FAST_PLAYBACK_FORMAT            DEFAULT_MEDIA_FORMAT
#define FAST_PLAYBACK_START            (FAST_PLAYBACK_PERIOD_SIZE * FAST_PLAYBACK_PERIOD_COUNT)
#define FAST_PLAYBACK_STOP              UINT_MAX

const struct pcm_config pcm_config_fast_playback = {
    .channels        = FAST_PLAYBACK_CHANNELS,
    .rate            = FAST_PLAYBACK_SAMPLING_RATE,
    .period_size     = FAST_PLAYBACK_PERIOD_SIZE,
    .period_count    = FAST_PLAYBACK_PERIOD_COUNT,
    .format          = FAST_PLAYBACK_FORMAT,
    .start_threshold = FAST_PLAYBACK_START,
    .stop_threshold  = FAST_PLAYBACK_STOP,
};

// PCM Configurations for DeepBuffer Playback Stream
#define DEEP_PLAYBACK_CARD              SOUND_CARD0
#define DEEP_PLAYBACK_DEVICE            SOUND_DEVICE_VIRT_DEEP_PLAYBACK
#define DEEP_PLAYBACK_IS_MMAP           false

#define DEEP_PLAYBACK_CHANNELS          DEFAULT_MEDIA_CHANNELS
#define DEEP_PLAYBACK_SAMPLING_RATE     DEFAULT_MEDIA_SAMPLING_RATE
#define DEEP_PLAYBACK_PERIOD_SIZE       960
#define DEEP_PLAYBACK_PERIOD_COUNT      4
#define DEEP_PLAYBACK_FORMAT            DEFAULT_MEDIA_FORMAT
#define DEEP_PLAYBACK_START             DEEP_PLAYBACK_PERIOD_SIZE
#define DEEP_PLAYBACK_STOP              (DEEP_PLAYBACK_PERIOD_SIZE * DEEP_PLAYBACK_PERIOD_COUNT)

const struct pcm_config pcm_config_deep_playback = {
    .channels        = DEEP_PLAYBACK_CHANNELS,
    .rate            = DEEP_PLAYBACK_SAMPLING_RATE,
    .period_size     = DEEP_PLAYBACK_PERIOD_SIZE,
    .period_count    = DEEP_PLAYBACK_PERIOD_COUNT,
    .format          = DEEP_PLAYBACK_FORMAT,
    .start_threshold = DEEP_PLAYBACK_START,
    .stop_threshold  = DEEP_PLAYBACK_STOP,
};

// PCM Configurations for Deep UHQA Playback Stream
#define DEEP_PLAYBACK_UHQA_SAMPLING_RATE     UHQA_MEDIA_SAMPLING_RATE

const struct pcm_config pcm_config_deep_playback_uhqa = {
    .channels        = DEEP_PLAYBACK_CHANNELS,
    .rate            = DEEP_PLAYBACK_UHQA_SAMPLING_RATE,
    .period_size     = DEEP_PLAYBACK_PERIOD_SIZE * 4,
    .period_count    = DEEP_PLAYBACK_PERIOD_COUNT,
    .format          = DEEP_PLAYBACK_FORMAT,
};

/* PCM Configurations for Deep HiFi Playback Stream */
#define DEEP_PLAYBACK_HIFI_SAMPLING_RATE 44100

const struct pcm_config pcm_config_deep_playback_hifi = {
    .channels        = DEEP_PLAYBACK_CHANNELS,
    .rate            = DEEP_PLAYBACK_HIFI_SAMPLING_RATE,
    .period_size     = 880,
    .period_count    = DEEP_PLAYBACK_PERIOD_COUNT,
    .format          = DEEP_PLAYBACK_FORMAT,
};

// PCM Configurations for Low Latency Playback Stream
#define LOW_PLAYBACK_CARD               SOUND_CARD0
#define LOW_PLAYBACK_DEVICE             SOUND_DEVICE_VIRT_FAST_PLAYBACK
#define LOW_PLAYBACK_IS_MMAP            false

#define LOW_PLAYBACK_CHANNELS           DEFAULT_MEDIA_CHANNELS
#define LOW_PLAYBACK_SAMPLING_RATE      DEFAULT_MEDIA_SAMPLING_RATE
#define LOW_PLAYBACK_PERIOD_SIZE        96
#define LOW_PLAYBACK_PERIOD_COUNT       4
#define LOW_PLAYBACK_FORMAT             DEFAULT_MEDIA_FORMAT
#define LOW_PLAYBACK_START              LOW_PLAYBACK_PERIOD_SIZE
#define LOW_PLAYBACK_STOP               UINT_MAX

const struct pcm_config pcm_config_low_playback = {
    .channels        = LOW_PLAYBACK_CHANNELS,
    .rate            = LOW_PLAYBACK_SAMPLING_RATE,
    .period_size     = LOW_PLAYBACK_PERIOD_SIZE,
    .period_count    = LOW_PLAYBACK_PERIOD_COUNT,
    .format          = LOW_PLAYBACK_FORMAT,
    .start_threshold = LOW_PLAYBACK_START,
    .stop_threshold  = LOW_PLAYBACK_STOP,
};

// PCM Configurations for MMAP Playback Stream
#define MMAP_PLAYBACK_CARD               SOUND_CARD0
#define MMAP_PLAYBACK_DEVICE             SOUND_DEVICE_ABOX_RDMA2
#define MMAP_PLAYBACK_IS_MMAP            true

#define MMAP_PLAYBACK_CHANNELS           DEFAULT_MEDIA_CHANNELS
#define MMAP_PLAYBACK_SAMPLING_RATE      DEFAULT_MEDIA_SAMPLING_RATE
#define MMAP_PLAYBACK_PERIOD_SIZE        MMAP_PERIOD_SIZE
#define MMAP_PLAYBACK_PERIOD_COUNT       4
#define MMAP_PLAYBACK_FORMAT             DEFAULT_MEDIA_FORMAT
#define MMAP_PLAYBACK_START              MMAP_PLAYBACK_PERIOD_SIZE
#define MMAP_PLAYBACK_STOP               UINT_MAX

const struct pcm_config pcm_config_mmap_playback = {
    .channels        = MMAP_PLAYBACK_CHANNELS,
    .rate            = MMAP_PLAYBACK_SAMPLING_RATE,
    .period_size     = MMAP_PLAYBACK_PERIOD_SIZE,
    .period_count    = MMAP_PLAYBACK_PERIOD_COUNT,
    .format          = MMAP_PLAYBACK_FORMAT,
    .start_threshold = MMAP_PLAYBACK_START,
    .stop_threshold  = MMAP_PLAYBACK_STOP,
};

// PCM Configurations for Voice RX Playback Stream
#define VRX_PLAYBACK_CARD               SOUND_CARD0
#define VRX_PLAYBACK_DEVICE             SOUND_DEVICE_VIRT_VRX_PLAYBACK

#define VRX_PLAYBACK_CHANNELS           DEFAULT_VOICE_CHANNELS
#define VRX_PLAYBACK_SAMPLING_RATE      DEFAULT_VOICE_SAMPLING_RATE
#define VRX_PLAYBACK_PERIOD_SIZE        480
#define VRX_PLAYBACK_PERIOD_COUNT       4
#define VRX_PLAYBACK_FORMAT             DEFAULT_VOICE_FORMAT
#define VRX_PLAYBACK_START              VRX_PLAYBACK_PERIOD_SIZE
#define VRX_PLAYBACK_STOP               UINT_MAX

const struct pcm_config pcm_config_voicerx_playback = {
    .channels        = VRX_PLAYBACK_CHANNELS,
    .rate            = VRX_PLAYBACK_SAMPLING_RATE,
    .period_size     = VRX_PLAYBACK_PERIOD_SIZE,
    .period_count    = VRX_PLAYBACK_PERIOD_COUNT,
    .format          = VRX_PLAYBACK_FORMAT,
    .start_threshold = VRX_PLAYBACK_START,
    .stop_threshold  = VRX_PLAYBACK_STOP,
};

// PCM Configurations for VOIP Playback Stream
#define VOIP_PLAYBACK_CARD           SOUND_CARD0
#define VOIP_PLAYBACK_DEVICE         SOUND_DEVICE_VIRT_VOIP_PLAYBACK
#define VOIP_PLAYBACK_IS_MMAP        false

#define VOIP_PLAYBACK_CHANNELS       DEFAULT_MEDIA_CHANNELS
#define VOIP_PLAYBACK_SAMPLING_RATE  DEFAULT_MEDIA_SAMPLING_RATE
#define VOIP_PLAYBACK_PERIOD_SIZE    960
#define VOIP_PLAYBACK_PERIOD_COUNT   4
#define VOIP_PLAYBACK_FORMAT         DEFAULT_MEDIA_FORMAT
#define VOIP_PLAYBACK_START          VOIP_PLAYBACK_PERIOD_SIZE
#define VOIP_PLAYBACK_STOP           (VOIP_PLAYBACK_PERIOD_SIZE * VOIP_PLAYBACK_PERIOD_COUNT)

const struct pcm_config pcm_config_voip_playback = {
    .channels        = VOIP_PLAYBACK_CHANNELS,
    .rate            = VOIP_PLAYBACK_SAMPLING_RATE,
    .period_size     = VOIP_PLAYBACK_PERIOD_SIZE,
    .period_count    = VOIP_PLAYBACK_PERIOD_COUNT,
    .format          = VOIP_PLAYBACK_FORMAT,
    .start_threshold = VOIP_PLAYBACK_START,
    .stop_threshold  = VOIP_PLAYBACK_STOP,
};

// PCM Configurations for Compress Offload Playback Stream
#define OFFLOAD_PLAYBACK_CARD           SOUND_CARD0
#define OFFLOAD_PLAYBACK_DEVICE         SOUND_DEVICE_ABOX_RDMA1
#define OFFLOAD_PLAYBACK_IS_MMAP        false

/*
 * These values are based on HW Decoder: Max Buffer Size = FRAGMENT_SIZE * NUM_FRAGMENTS
 * 0 means that we will use the predefined value by device driver
 */
#define OFFLOAD_PLAYBACK_BUFFER_SIZE  (1024 * 64)  // fragment_size is fixed 64KBytes = 64 * 1024
#define OFFLOAD_PLAYBACK_BUFFER_COUNT  5           // fragment is fixed 5

#define OFFLOAD_OFFLOAD_FRAGMENT_SIZE OFFLOAD_PLAYBACK_BUFFER_SIZE
#define OFFLOAD_OFFLOAD_NUM_FRAGMENTS OFFLOAD_PLAYBACK_BUFFER_COUNT

const struct compr_config compr_config_offload_playback = {
    .fragment_size  = OFFLOAD_OFFLOAD_FRAGMENT_SIZE,
    .fragments      = OFFLOAD_OFFLOAD_NUM_FRAGMENTS,
    .codec          = NULL,
};

// PCM Configurations for SpeakerAMP Playback Stream
#define SPKAMP_PLAYBACK_CARD            SOUND_CARD0
#define SPKAMP_PLAYBACK_DEVICE          SOUND_DEVICE_ABOX_RDMA7

#define SPKAMP_PLAYBACK_CHANNELS        DEFAULT_MEDIA_CHANNELS
#define SPKAMP_PLAYBACK_SAMPLING_RATE   DEFAULT_MEDIA_SAMPLING_RATE
#define SPKAMP_PLAYBACK_PERIOD_SIZE     480
#define SPKAMP_PLAYBACK_PERIOD_COUNT    4
#define SPKAMP_PLAYBACK_FORMAT          DEFAULT_MEDIA_FORMAT
#define SPKAMP_PLAYBACK_START           SPKAMP_PLAYBACK_PERIOD_SIZE
#define SPKAMP_PLAYBACK_STOP            UINT_MAX

const struct pcm_config pcm_config_spkamp_playback = {
    .channels        = SPKAMP_PLAYBACK_CHANNELS,
    .rate            = SPKAMP_PLAYBACK_SAMPLING_RATE,
    .period_size     = SPKAMP_PLAYBACK_PERIOD_SIZE,
    .period_count    = SPKAMP_PLAYBACK_PERIOD_COUNT,
    .format          = SPKAMP_PLAYBACK_FORMAT,
    .start_threshold = SPKAMP_PLAYBACK_START,
    .stop_threshold  = SPKAMP_PLAYBACK_STOP,
};

// PCM Configurations for Incall Music Playback Stream
#define INCALLMUSIC_PLAYBACK_CARD              SOUND_CARD0
#define INCALLMUSIC_PLAYBACK_DEVICE            SOUND_DEVICE_VIRT_INCALLMUSIC_PLAYBACK
#define INCALLMUSIC_PLAYBACK_IS_MMAP           false

#define INCALLMUSIC_PLAYBACK_CHANNELS          DEFAULT_MEDIA_CHANNELS
#define INCALLMUSIC_PLAYBACK_SAMPLING_RATE     DEFAULT_MEDIA_SAMPLING_RATE
#define INCALLMUSIC_PLAYBACK_PERIOD_SIZE       480
#define INCALLMUSIC_PLAYBACK_PERIOD_COUNT      4
#define INCALLMUSIC_PLAYBACK_FORMAT            DEFAULT_MEDIA_FORMAT
#define INCALLMUSIC_PLAYBACK_START             (INCALLMUSIC_PLAYBACK_PERIOD_SIZE * INCALLMUSIC_PLAYBACK_PERIOD_COUNT)
#define INCALLMUSIC_PLAYBACK_STOP              UINT_MAX

const struct pcm_config pcm_config_incallmusic_playback = {
    .channels        = INCALLMUSIC_PLAYBACK_CHANNELS,
    .rate            = INCALLMUSIC_PLAYBACK_SAMPLING_RATE,
    .period_size     = INCALLMUSIC_PLAYBACK_PERIOD_SIZE,
    .period_count    = INCALLMUSIC_PLAYBACK_PERIOD_COUNT,
    .format          = INCALLMUSIC_PLAYBACK_FORMAT,
    .start_threshold = INCALLMUSIC_PLAYBACK_START,
    .stop_threshold  = INCALLMUSIC_PLAYBACK_STOP,
};

// PCM Configurations for Mute Playback Stream
#define MUTE_PLAYBACK_CARD              SOUND_CARD0
#define MUTE_PLAYBACK_DEVICE            SOUND_DEVICE_VIRT_MUTE_PLAYBACK

#define MUTE_PLAYBACK_CHANNELS          DEFAULT_MEDIA_CHANNELS
#define MUTE_PLAYBACK_SAMPLING_RATE     DEFAULT_MEDIA_SAMPLING_RATE
#define MUTE_PLAYBACK_PERIOD_SIZE       480
#define MUTE_PLAYBACK_PERIOD_COUNT      4
#define MUTE_PLAYBACK_FORMAT            DEFAULT_MEDIA_FORMAT
#define MUTE_PLAYBACK_START             MUTE_PLAYBACK_PERIOD_SIZE
#define MUTE_PLAYBACK_STOP              UINT_MAX

const struct pcm_config pcm_config_mute_playback = {
    .channels        = MUTE_PLAYBACK_CHANNELS,
    .rate            = MUTE_PLAYBACK_SAMPLING_RATE,
    .period_size     = MUTE_PLAYBACK_PERIOD_SIZE,
    .period_count    = MUTE_PLAYBACK_PERIOD_COUNT,
    .format          = MUTE_PLAYBACK_FORMAT,
    .start_threshold = MUTE_PLAYBACK_START,
    .stop_threshold  = MUTE_PLAYBACK_STOP,
};

// PCM Configurations for AUX Digital(HDMI / DisplayPort) Playback Stream
#define AUX_PLAYBACK_CARD               SOUND_CARD0
#define AUX_PLAYBACK_DEVICE             SOUND_DEVICE_AUX
#define AUX_PLAYBACK_IS_MMAP            false

#define AUX_PLAYBACK_CHANNELS           DEFAULT_MEDIA_CHANNELS
#define AUX_PLAYBACK_SAMPLING_RATE      DEFAULT_MEDIA_SAMPLING_RATE
#define AUX_PLAYBACK_PERIOD_SIZE        960
#define AUX_PLAYBACK_PERIOD_COUNT       2
#define AUX_PLAYBACK_FORMAT             DEFAULT_MEDIA_FORMAT
#define AUX_PLAYBACK_START              AUX_PLAYBACK_PERIOD_SIZE
#define AUX_PLAYBACK_STOP               UINT_MAX

const struct pcm_config pcm_config_aux_playback = {
    .channels        = AUX_PLAYBACK_CHANNELS,
    .rate            = AUX_PLAYBACK_SAMPLING_RATE,
    .period_size     = AUX_PLAYBACK_PERIOD_SIZE,
    .period_count    = AUX_PLAYBACK_PERIOD_COUNT,
    .format          = AUX_PLAYBACK_FORMAT,
    .start_threshold = AUX_PLAYBACK_START,
    .stop_threshold  = AUX_PLAYBACK_STOP,
};

// PCM Configurations for UltraSound RX
#define ULTRASOUND_PLAYBACK_CARD               SOUND_CARD0
#define ULTRASOUND_PLAYBACK_DEVICE             SOUND_DEVICE_VIRT_ULTRASOUND_PLAYBACK

#define ULTRASOUND_PLAYBACK_CHANNELS           DEFAULT_MEDIA_CHANNELS
#define ULTRASOUND_PLAYBACK_SAMPLING_RATE      DEFAULT_MEDIA_SAMPLING_RATE
#define ULTRASOUND_PLAYBACK_PERIOD_SIZE        960  // 20ms
#define ULTRASOUND_PLAYBACK_PERIOD_COUNT       4
#define ULTRASOUND_PLAYBACK_FORMAT             DEFAULT_MEDIA_FORMAT
#define ULTRASOUND_PLAYBACK_START              ULTRASOUND_PLAYBACK_PERIOD_SIZE
#define ULTRASOUND_PLAYBACK_STOP               UINT_MAX

const struct pcm_config pcm_config_ultrasound_playback = {
    .channels        = ULTRASOUND_PLAYBACK_CHANNELS,
    .rate            = ULTRASOUND_PLAYBACK_SAMPLING_RATE,
    .period_size     = ULTRASOUND_PLAYBACK_PERIOD_SIZE,
    .period_count    = ULTRASOUND_PLAYBACK_PERIOD_COUNT,
    .format          = ULTRASOUND_PLAYBACK_FORMAT,
    .start_threshold = ULTRASOUND_PLAYBACK_START,
    .stop_threshold  = ULTRASOUND_PLAYBACK_STOP,
};

//----------------------------------------------------------------------------------------------//
// For Capture (MIC) Path

// PCM Configurations for Primary Capture Stream
#define PRIMARY_CAPTURE_CARD            SOUND_CARD0
#define PRIMARY_CAPTURE_DEVICE          SOUND_DEVICE_VIRT_PRIMARY_CAPTURE

#define PRIMARY_CAPTURE_CHANNELS        DEFAULT_MEDIA_CHANNELS
#define PRIMARY_CAPTURE_SAMPLING_RATE   DEFAULT_MEDIA_SAMPLING_RATE
#define PRIMARY_CAPTURE_PERIOD_SIZE     960
#define PRIMARY_CAPTURE_PERIOD_COUNT    4
#define PRIMARY_CAPTURE_FORMAT          DEFAULT_MEDIA_FORMAT
#define PRIMARY_CAPTURE_START           PRIMARY_CAPTURE_PERIOD_SIZE
#define PRIMARY_CAPTURE_STOP            UINT_MAX

const struct pcm_config pcm_config_primary_capture = {
    .channels        = PRIMARY_CAPTURE_CHANNELS,
    .rate            = PRIMARY_CAPTURE_SAMPLING_RATE,
    .period_size     = PRIMARY_CAPTURE_PERIOD_SIZE,
    .period_count    = PRIMARY_CAPTURE_PERIOD_COUNT,
    .format          = PRIMARY_CAPTURE_FORMAT,
    .start_threshold = PRIMARY_CAPTURE_START,
    .stop_threshold  = PRIMARY_CAPTURE_STOP,
};

// PCM Configurations for Primary Capture Stream
#define VOIP_CAPTURE_CARD               SOUND_CARD0
#define VOIP_CAPTURE_DEVICE             SOUND_DEVICE_VIRT_VOIP_CAPTURE

#define VOIP_CAPTURE_CHANNELS           DEFAULT_MEDIA_CHANNELS
#define VOIP_CAPTURE_SAMPLING_RATE      DEFAULT_MEDIA_SAMPLING_RATE
#define VOIP_CAPTURE_PERIOD_SIZE        960
#define VOIP_CAPTURE_PERIOD_COUNT       4
#define VOIP_CAPTURE_FORMAT             DEFAULT_MEDIA_FORMAT
#define VOIP_CAPTURE_START              PRIMARY_CAPTURE_PERIOD_SIZE
#define VOIP_CAPTURE_STOP               UINT_MAX

const struct pcm_config pcm_config_voip_capture = {
    .channels        = VOIP_CAPTURE_CHANNELS,
    .rate            = VOIP_CAPTURE_SAMPLING_RATE,
    .period_size     = VOIP_CAPTURE_PERIOD_SIZE,
    .period_count    = VOIP_CAPTURE_PERIOD_COUNT,
    .format          = VOIP_CAPTURE_FORMAT,
    .start_threshold = VOIP_CAPTURE_START,
    .stop_threshold  = VOIP_CAPTURE_STOP,
};

// PCM Configurations for Low Latency Capture Stream
#define LOW_CAPTURE_CARD                SOUND_CARD0
#define LOW_CAPTURE_DEVICE              SOUND_DEVICE_VIRT_LOWLATENCY_CAPTURE

#define LOW_CAPTURE_CHANNELS            DEFAULT_MEDIA_CHANNELS
#define LOW_CAPTURE_SAMPLING_RATE       DEFAULT_MEDIA_SAMPLING_RATE
#define LOW_CAPTURE_PERIOD_SIZE         240
#define LOW_CAPTURE_PERIOD_COUNT        16
#define LOW_CAPTURE_FORMAT              DEFAULT_MEDIA_FORMAT
#define LOW_CAPTURE_START               LOW_CAPTURE_PERIOD_SIZE
#define LOW_CAPTURE_STOP                UINT_MAX

const struct pcm_config pcm_config_low_capture = {
    .channels        = LOW_CAPTURE_CHANNELS,
    .rate            = LOW_CAPTURE_SAMPLING_RATE,
    .period_size     = LOW_CAPTURE_PERIOD_SIZE,
    .period_count    = LOW_CAPTURE_PERIOD_COUNT,
    .format          = LOW_CAPTURE_FORMAT,
    .start_threshold = LOW_CAPTURE_START,
    .stop_threshold  = LOW_CAPTURE_STOP,
};

// PCM Configurations for MMAP Capture Stream
#define MMAP_CAPTURE_CARD               SOUND_CARD0
#define MMAP_CAPTURE_DEVICE             SOUND_DEVICE_ABOX_WDMA1

#define MMAP_CAPTURE_CHANNELS           DEFAULT_MEDIA_CHANNELS
#define MMAP_CAPTURE_SAMPLING_RATE      DEFAULT_MEDIA_SAMPLING_RATE
#define MMAP_CAPTURE_PERIOD_SIZE        MMAP_PERIOD_SIZE
#define MMAP_CAPTURE_PERIOD_COUNT       4
#define MMAP_CAPTURE_FORMAT             DEFAULT_MEDIA_FORMAT
#define MMAP_CAPTURE_START              MMAP_CAPTURE_PERIOD_SIZE
#define MMAP_CAPTURE_STOP               UINT_MAX

const struct pcm_config pcm_config_mmap_capture = {
    .channels        = MMAP_CAPTURE_CHANNELS,
    .rate            = MMAP_CAPTURE_SAMPLING_RATE,
    .period_size     = MMAP_CAPTURE_PERIOD_SIZE,
    .period_count    = MMAP_CAPTURE_PERIOD_COUNT,
    .format          = MMAP_CAPTURE_FORMAT,
    .start_threshold = MMAP_CAPTURE_START,
    .stop_threshold  = MMAP_CAPTURE_STOP,
};

// PCM Configurations for Voice TX Capture Stream
#define VTX_CAPTURE_CARD                SOUND_CARD0
#define VTX_CAPTURE_DEVICE              SOUND_DEVICE_VIRT_VTX_CAPTURE

#define VTX_CAPTURE_CHANNELS            DEFAULT_VOICE_CHANNELS
#define VTX_CAPTURE_SAMPLING_RATE       DEFAULT_VOICE_SAMPLING_RATE
#define VTX_CAPTURE_PERIOD_SIZE         960
#define VTX_CAPTURE_PERIOD_COUNT        4
#define VTX_CAPTURE_FORMAT              DEFAULT_VOICE_FORMAT
#define VTX_CAPTURE_START               VTX_CAPTURE_PERIOD_SIZE
#define VTX_CAPTURE_STOP                UINT_MAX

const struct pcm_config pcm_config_voicetx_capture = {
    .channels        = VTX_CAPTURE_CHANNELS,
    .rate            = VTX_CAPTURE_SAMPLING_RATE,
    .period_size     = VTX_CAPTURE_PERIOD_SIZE,
    .period_count    = VTX_CAPTURE_PERIOD_COUNT,
    .format          = VTX_CAPTURE_FORMAT,
    .start_threshold = VTX_CAPTURE_START,
    .stop_threshold  = VTX_CAPTURE_STOP,
};

// PCM Configurations for Speaker AMP Reference Stream
#define SPKAMP_REFERENCE_CARD           SOUND_CARD0
#define SPKAMP_REFERENCE_DEVICE         SOUND_DEVICE_VIRT_TRIGGER

#define SPKAMP_REFERENCE_CHANNELS       DEFAULT_MEDIA_CHANNELS
#define SPKAMP_REFERENCE_SAMPLING_RATE  DEFAULT_MEDIA_SAMPLING_RATE
#define SPKAMP_REFERENCE_PERIOD_SIZE    240
#define SPKAMP_REFERENCE_PERIOD_COUNT   4
#define SPKAMP_REFERENCE_FORMAT         DEFAULT_MEDIA_FORMAT
#define SPKAMP_REFERENCE_START          SPKAMP_REFERENCE_PERIOD_SIZE
#define SPKAMP_REFERENCE_STOP           UINT_MAX

const struct pcm_config pcm_config_spkamp_reference = {
    .channels        = SPKAMP_REFERENCE_CHANNELS,
    .rate            = SPKAMP_REFERENCE_SAMPLING_RATE,
    .period_size     = SPKAMP_REFERENCE_PERIOD_SIZE,
    .period_count    = SPKAMP_REFERENCE_PERIOD_COUNT,
    .format          = SPKAMP_REFERENCE_FORMAT,
    .start_threshold = SPKAMP_REFERENCE_START,
    .stop_threshold  = SPKAMP_REFERENCE_STOP,
};

// PCM Configurations for Voice Call Recording Stream
#define CALL_RECORD_CARD                SOUND_CARD0
#define CALL_RECORD_DEVICE              SOUND_DEVICE_CALL_RECORD

#define CALL_RECORD_CHANNELS            DEFAULT_VOICE_REC_CHANNELS
#define CALL_RECORD_SAMPLING_RATE       DEFAULT_VOICE_REC_SAMPLINGRATE
#define CALL_RECORD_PERIOD_SIZE         DEFAULT_VOICE_REC_PERIODSIZE
#define CALL_RECORD_PERIOD_COUNT        DEFAULT_VOICE_REC_PERIODCOUNT
#define CALL_RECORD_FORMAT              DEFAULT_VOICE_REC_FORMAT
#define CALL_RECORD_START               CALL_RECORD_PERIOD_SIZE
#define CALL_RECORD_STOP                UINT_MAX

const struct pcm_config pcm_config_call_record = {
    .channels        = CALL_RECORD_CHANNELS,
    .rate            = CALL_RECORD_SAMPLING_RATE,
    .period_size     = CALL_RECORD_PERIOD_SIZE,
    .period_count    = CALL_RECORD_PERIOD_COUNT,
    .format          = CALL_RECORD_FORMAT,
    .start_threshold = CALL_RECORD_START,
    .stop_threshold  = CALL_RECORD_STOP,
};

// PCM Configurations for FM Radio Recording Stream
#define SOUND_DEVICE_VIRT_FM_RECORD     SOUND_DEVICE_VIRT_BARGEIN_CAPTURE

#define FM_RECORD_CARD                  SOUND_CARD0
#define FM_RECORD_DEVICE                SOUND_DEVICE_VIRT_FM_RECORD

#define FM_RECORD_CHANNELS              DEFAULT_MEDIA_CHANNELS
#define FM_RECORD_SAMPLING_RATE         DEFAULT_MEDIA_SAMPLING_RATE
#define FM_RECORD_PERIOD_SIZE           480
#define FM_RECORD_PERIOD_COUNT          4
#define FM_RECORD_FORMAT                DEFAULT_MEDIA_FORMAT
#define FM_RECORD_START                 FM_RECORD_PERIOD_SIZE
#define FM_RECORD_STOP                  UINT_MAX

const struct pcm_config pcm_config_fm_record = {
    .channels        = FM_RECORD_CHANNELS,
    .rate            = FM_RECORD_SAMPLING_RATE,
    .period_size     = FM_RECORD_PERIOD_SIZE,
    .period_count    = FM_RECORD_PERIOD_COUNT,
    .format          = FM_RECORD_FORMAT,
    .start_threshold = FM_RECORD_START,
    .stop_threshold  = FM_RECORD_STOP,
};


#ifdef SUPPORT_STHAL_INTERFACE
// PCM Configurations for hotword capture Stream
// Note: Should be matching with STHAL pcm configuration
#define DEFAULT_HOTWORD_CHANNELS                1       // Mono
#define DEFAULT_HOTWORD_SAMPLING_RATE           16000
#define HOTWORD_PERIOD_SIZE                     480     // 480 frames, 30ms in case of 16KHz Stream
#define HOTWORD_PERIOD_COUNT                    128     // Buffer count => Total  122880 Bytes = 480 * 1(Mono) * 2(16bit PCM) * 128(Buffer count)

const struct pcm_config pcm_config_hotword_capture = {
    .channels = DEFAULT_HOTWORD_CHANNELS,
    .rate = DEFAULT_HOTWORD_SAMPLING_RATE,
    .period_size = HOTWORD_PERIOD_SIZE,
    .period_count = HOTWORD_PERIOD_COUNT,
    .format = PCM_FORMAT_S16_LE,
};

#ifdef SUPPORT_BARGEIN_MODE
#define BARGEIN_TRIGGER_CARD            SOUND_CARD0
#define BARGEIN_DUMMY_BRIDGE_DEVICE     SOUND_DEVICE_VIRT_BARGEIN_DUMMY_CAP
#define BARGEIN_TRIGGER_DEVICE          SOUND_DEVICE_VIRT_BARGEIN_TRIGGER

#define BARGEIN_SAMPLE_RATE             48000
#define BARGEIN_TRIGGER_SAMPLE_RATE     BARGEIN_SAMPLE_RATE
#define BARGEIN_TRIGGER_CHANNELS        2
#define BARGEIN_TRIGGER_PERIOD_SIZE    (BARGEIN_SAMPLE_RATE / 50)  // 20ms
#define BARGEIN_TRIGGER_PERIOD_COUNT    10
#define BARGEIN_TRIGGER_FORMAT          PCM_FORMAT_S16_LE
#define BARGEIN_TRIGGER_STOP            UINT_MAX

static struct pcm_config pcm_config_bargein_trigger = {
    .channels        = BARGEIN_TRIGGER_CHANNELS,
    .rate            = BARGEIN_TRIGGER_SAMPLE_RATE,
    .period_size     = BARGEIN_TRIGGER_PERIOD_SIZE,
    .period_count    = BARGEIN_TRIGGER_PERIOD_COUNT,
    .format = BARGEIN_TRIGGER_FORMAT,
    .stop_threshold = BARGEIN_TRIGGER_STOP,
};
#endif
#endif

// PCM Configurations for Ultrasound TxStream
#define ULTRASOUND_CAPTURE_CARD               SOUND_CARD0
#define ULTRASOUND_CAPTURE_DEVICE             SOUND_DEVICE_VIRT_ULTRASOUND_CAPTURE

#define ULTRASOUND_CAPTURE_CHANNELS           DEFAULT_MEDIA_CHANNELS
#define ULTRASOUND_CAPTURE_SAMPLING_RATE      DEFAULT_MEDIA_SAMPLING_RATE
#define ULTRASOUND_CAPTURE_PERIOD_SIZE        960  // 20ms
#define ULTRASOUND_CAPTURE_PERIOD_COUNT       4
#define ULTRASOUND_CAPTURE_FORMAT             DEFAULT_MEDIA_FORMAT

const struct pcm_config pcm_config_ultrasound_capture = {
    .channels     = ULTRASOUND_CAPTURE_CHANNELS,
    .rate         = ULTRASOUND_CAPTURE_SAMPLING_RATE,
    .period_size  = ULTRASOUND_CAPTURE_PERIOD_SIZE,
    .period_count = ULTRASOUND_CAPTURE_PERIOD_COUNT,
    .format       = ULTRASOUND_CAPTURE_FORMAT,
};

#define MAX_PCM_PATH_LEN 256

// Duration for DP Playback
#define PREDEFINED_DP_PLAYBACK_DURATION     20  // 20ms

#endif  // __EXYNOS_AUDIOPROXY_PCM_H__
