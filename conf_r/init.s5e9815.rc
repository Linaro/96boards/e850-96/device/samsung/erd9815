on early-init
    mkdir /mnt/vendor/efs 0771 radio system
    mkdir /mnt/vendor/persist 0771 system system
    mount debugfs /sys/kernel/debug /sys/kernel/debug
    setprop ro.soc.manufacturer Samsung
    setprop ro.soc.model Exynos\ 9815

on init
    # See storage config details at http://source.android.com/tech/storage/
    mkdir /storage/sdcard 0000 root root
    mkdir /storage/usb1 0000 root root
    mkdir /storage/usb2 0000 root root
    mkdir /mnt/media_rw/sdcard 0700 media_rw media_rw
    mkdir /mnt/media_rw/usb1 0700 media_rw media_rw
    mkdir /mnt/media_rw/usb2 0700 media_rw media_rw

    export SECONDARY_STORAGE /storage/sdcard:/storage/usb1:/storage/usb2

    start watchdogd

    # Support legacy paths
    symlink /storage/sdcard /mnt/ext_sd
    symlink /data/app /factory
    ## SCSC WLAN
    # SD-755: Increase default socket rx buffer limit
    # Apply network parameters for high data performance.
    write /proc/sys/net/core/rmem_default 327680
    write /proc/sys/net/core/rmem_max 8388608
    write /proc/sys/net/core/wmem_default 2805760
    write /proc/sys/net/core/wmem_max 8388608
    write /proc/sys/net/core/optmem_max 20480
    write /proc/sys/net/core/netdev_max_backlog 100000
    write /proc/sys/net/ipv4/tcp_rmem "2097152 4194304 8388608"
    write /proc/sys/net/ipv4/tcp_wmem "262144 524288 1048576"
    write /proc/sys/net/ipv4/tcp_mem "44259 59012 88518"
    write /proc/sys/net/ipv4/udp_mem "88518 118025 177036"

    write /sys/class/net/rmnet0/queues/rx-0/rps_cpus fe
    write /sys/class/net/rmnet1/queues/rx-0/rps_cpus fe
    write /sys/class/net/rmnet2/queues/rx-0/rps_cpus fe
    write /sys/class/net/rmnet3/queues/rx-0/rps_cpus fe
    write /sys/class/net/rmnet4/queues/rx-0/rps_cpus fe
    write /sys/class/net/rmnet5/queues/rx-0/rps_cpus fe
    write /sys/class/net/rmnet6/queues/rx-0/rps_cpus fe
    write /sys/class/net/rmnet7/queues/rx-0/rps_cpus fe

    ## SCSC BT
    chown bluetooth bluetooth /sys/module/scsc_bt/parameters/bluetooth_address

    write /sys/devices/system/cpu/cpufreq/policy0/scaling_governor energy_step
    write /sys/devices/system/cpu/cpufreq/policy4/scaling_governor energy_step
    write /sys/devices/system/cpu/cpufreq/policy7/scaling_governor energy_step

    #
    # EAS uclamp interfaces
    #
    mkdir /dev/cpuctl/foreground
    mkdir /dev/cpuctl/background
    mkdir /dev/cpuctl/top-app
    chown system system /dev/cpuctl
    chown system system /dev/cpuctl/foreground
    chown system system /dev/cpuctl/background
    chown system system /dev/cpuctl/top-app
    chown system system /dev/cpuctl/tasks
    chown system system /dev/cpuctl/foreground/tasks
    chown system system /dev/cpuctl/background/tasks
    chown system system /dev/cpuctl/top-app/tasks
    chmod 0664 /dev/cpuctl/tasks
    chmod 0664 /dev/cpuctl/foreground/tasks
    chmod 0664 /dev/cpuctl/background/tasks
    chmod 0664 /dev/cpuctl/top-app/tasks
    write /dev/cpuctl/foreground/cpu.rt_runtime_us 950000
    write /dev/cpuctl/background/cpu.rt_runtime_us 950000
    write /dev/cpuctl/top-app/cpu.rt_runtime_us 950000

    # EPIC interfaces
    chmod 660 /dev/stune/top-app/schedtune.boost
    chown system system /dev/mode
    chown system system /sys/kernel/ems/global_boost
    chown system system /dev/exynos-migov
    chown system system /dev/exynos-sysbusy
    chown system system /sys/devices/platform/exynos-migov/control/control_profile
    chown system system /sys/devices/platform/exynos-migov/control/set_margin
    chown system system /sys/devices/platform/exynos-migov/control/fragutil_thr
    chown system system /sys/devices/platform/exynos-migov/migov/running
    chown system system /dev/cpu_dma_latency
    chown system system /dev/cluster0_freq_min
    chown system system /dev/cluster1_freq_min
    chown system system /dev/cluster2_freq_min
    chown system system /dev/cluster0_freq_max
    chown system system /dev/cluster1_freq_max
    chown system system /dev/cluster2_freq_max
    chown system system /dev/cpu_online_min
    chown system system /dev/cpu_online_max
    chown system system /dev/bus_throughput
    chown system system /dev/bus_throughput_max
    chown system system /dev/mfc_throughput
    chown system system /dev/mfc_throughput_max
    chown system system /dev/gpu_freq_min
    chown system system /dev/gpu_freq_max
    chown system system /dev/npu_throughput
    chown system system /dev/npu_throughput_max
    chown system system /dev/device_throughput
    chown system system /dev/device_throughput_max

    # EMS interfaces
    chown system system /sys/kernel/ems/emstune/req_mode
    chown system system /sys/kernel/ems/emstune/req_mode_level
    chown system system /sys/kernel/ems/emstune/aio_tuner
    chown system system /sys/kernel/ems/emstune/vip_thread
    chown system system /sys/kernel/ems/emstune/vip_del
    chown system system /sys/kernel/ems/ecs/req_cpus
    chown system system /sys/kernel/ems/energy_step/coregroup0/uclamp_min
    chown system system /sys/kernel/ems/energy_step/coregroup4/uclamp_min
    chown system system /sys/kernel/ems/energy_step/coregroup7/uclamp_min
    chown system system /sys/kernel/ems/energy_step/coregroup0/uclamp_max
    chown system system /sys/kernel/ems/energy_step/coregroup4/uclamp_max
    chown system system /sys/kernel/ems/energy_step/coregroup7/uclamp_max

    # ALT interface
    chown system system /sys/class/devfreq/17000010.devfreq_mif/interactive/current_mode

    # universal governor
    chown system system /sys/kernel/exynos_perf_cpufreq/gov
    chown system system /sys/kernel/exynos_perf_cpufreq/run
    chmod 0664 /sys/kernel/exynos_perf_cpufreq/gov
    chmod 0664 /sys/kernel/exynos_perf_cpufreq/run

    chown system system /dev/cpuset/tasks
    chown system system /dev/cpuset/cgroup.procs
    chmod 0664 /dev/cpuset/tasks

    chmod 0664 /dev/nanohub
    chmod 0664 /dev/nanohub_comms
    chmod 0664 /dev/chub_dev
    chown system system /dev/nanohub
    chown system system /dev/nanohub_comms
    chown system system /dev/chub_dev

    #Exynos Thermal Daemon
    chown system system /sys/class/power_supply/battery/thermal_limit
    chown system system /sys/devices/platform/18500000.mali/dvfs_max_lock
    chown system system /sys/kernel/ems/ecs/req_cpus
    chown system system /sys/devices/platform/17090000.exynos_is/debug/fixed_sensor_fps
    chown system system /sys/devices/platform/17090000.exynos_is/debug/en_fixed_sensor_fps
    chown system system /sys/devices/platform/panel_0/thermal_max_brightness
    chown system system /dev/cluster0_freq_max
    chown system system /dev/cluster1_freq_max

# Apply network parameters for high data performance.
    write /proc/sys/net/core/rmem_default 327680
    write /proc/sys/net/core/rmem_max 8388608
    write /proc/sys/net/core/wmem_default 327680
    write /proc/sys/net/core/wmem_max 8388608
    write /proc/sys/net/core/optmem_max 20480
    write /proc/sys/net/core/netdev_max_backlog 100000
    write /proc/sys/net/ipv4/tcp_rmem "2097152 4194304 8388608"
    write /proc/sys/net/ipv4/tcp_wmem "262144 524288 1048576"
    write /proc/sys/net/ipv4/tcp_mem "44259 59012 88518"
    write /proc/sys/net/ipv4/udp_mem "88518 118025 177036"

    write /sys/class/net/rmnet0/queues/rx-0/rps_cpus fe
    write /sys/class/net/rmnet1/queues/rx-0/rps_cpus fe
    write /sys/class/net/rmnet2/queues/rx-0/rps_cpus fe
    write /sys/class/net/rmnet3/queues/rx-0/rps_cpus fe
    write /sys/class/net/rmnet4/queues/rx-0/rps_cpus fe
    write /sys/class/net/rmnet5/queues/rx-0/rps_cpus fe
    write /sys/class/net/rmnet6/queues/rx-0/rps_cpus fe
    write /sys/class/net/rmnet7/queues/rx-0/rps_cpus fe

# Permission for GPU
    chown mediacodec system /sys/kernel/gpu/gpu_poweroff_delay
    chmod 0664 /sys/kernel/gpu/gpu_poweroff_delay
    chown mediacodec system /sys/kernel/gpu/gpu_mm_min_clock
    chmod 0664 /sys/kernel/gpu/gpu_mm_min_clock
    chown system system /sys/devices/system/cpu/cpufreq/policy0/scaling_min_freq
    chmod 0644 /sys/devices/system/cpu/cpufreq/policy0/scaling_min_freq
    chown system system /sys/devices/system/cpu/cpufreq/policy0/scaling_max_freq
    chmod 0644 /sys/devices/system/cpu/cpufreq/policy0/scaling_max_freq
    chown system system /sys/devices/system/cpu/cpufreq/policy4/scaling_min_freq
    chmod 0644 /sys/devices/system/cpu/cpufreq/policy4/scaling_min_freq
    chown system system /sys/devices/system/cpu/cpufreq/policy4/scaling_max_freq
    chmod 0644 /sys/devices/system/cpu/cpufreq/policy4/scaling_max_freq

on post-fs
    # set RLIMIT_MEMLOCK to 64MB
    setrlimit 8 67108864 67108864

on post-fs-data
    mkdir /data/vendor/gps 0771 system system

# IPSEC PIDDIR for VoWiFi
    mkdir /data/vendor/misc 0771 root system
    mkdir /data/vendor/misc/vpn 0771 radio system

# Log data folder
    mkdir /data/vendor 0771 root system
    mkdir /data/vendor/log 0771 root system
    mkdir /data/vendor/log/cbd 0771 root system
    mkdir /data/vendor/log/abox 0771 audioserver system
    mkdir /data/vendor/log/chub 0771 root system
    mkdir /data/vendor/log/gps 0771 system system

    mkdir /data/vendor/rild 0771 radio system
    mkdir /data/vendor/dump 0771 radio system
    mkdir /data/vendor/slog 0771 system log

    mkdir /data/vendor/thermal 0771 system system
    mkdir /data/vendor/thermal/config 0771 system system

    setprop vold.post_fs_data_done 1
    setprop sys.usb.controller 12210000.usb

# Directory for GPS
    rm /data/vendor/gps/gps_started
    rm /data/vendor/gps/glonass_started
    rm /data/vendor/gps/beidou_started
    rm /data/vendor/gps/smd_started
    rm /data/vendor/gps/sv_cno.info

## SCSC WLAN
    # Setup Wi-Fi permissions
    mkdir /data/vendor/wifi 0770 wifi system
    mkdir /data/vendor/wifi/wpa 0770 wifi system
    mkdir /data/vendor/wifi/wpa/sockets 0770 wifi wifi
    mkdir /data/vendor/wifi/hostapd/sockets 0770 wifi wifi
    mkdir /vendor/etc/wifi 0770 system system
    mkdir /data/vendor/log/wifi 0770 wifi wifi
    chown wifi wifi /sys/wifi/memdump
# Permissions Camera
    mkdir /data/camera 0777 root root

    chmod 0755 /sys/kernel/debug/tracing
    restorecon /sys/kernel/debug/tracing/trace_marker

# Gatekeeper data
    mkdir /data/vendor/gk 0771 system system

# Video data
    mkdir /data/vendor/media 0700 mediacodec mediacodec
    mkdir /data/vendor/media/gpu 0700 mediacodec mediacodec

# HWC data
    mkdir /data/vendor/log/hwc 0771 system graphics

# HDR xml data
    mkdir /data/vendor/hdr 0771 system system

# Eden data
    mkdir /data/vendor/eden 0771 system system
    mkdir /data/vendor/eden/gpu 0771 system system

on boot
    setprop wifi.interface wlan0



    # SCSC do not enable autorecovery in dev builds
    # write /sys/module/scsc_mx/parameters/disable_recovery_handling 0

    # Allow to access debugfs for system:system
    chmod 0755 /sys/kernel/debug
    chown system system /sys/kernel/debug

    setprop ro.radio.noril no

    # Set up kernel tracing, but disable it by default
    chmod 0222 /sys/kernel/debug/tracing/trace_marker
    write /sys/kernel/debug/tracing/tracing_on 0

    # Change permission for A-Box firmware logs file & GPR dump
    chown audioserver system /sys/devices/platform/16850000.abox/reset
    chown audioserver system /sys/devices/platform/16850000.abox/service
    chown audioserver system /sys/devices/platform/16850000.abox/0.abox-debug/gpr
    chown audioserver system /sys/devices/platform/16850000.abox/0.abox-debug/calliope_sram
    chown audioserver system /sys/devices/platform/16850000.abox/0.abox-debug/calliope_dram
    chown audioserver system /sys/devices/platform/16850000.abox/0.abox-debug/calliope_slog
    chown audioserver system /sys/kernel/debug/regmap/16850000.abox/registers
    chown audioserver system /sys/kernel/debug/abox/log-00

# Permission for USB SELECT
    chown system system /sys/class/android_usb/android0/enable
    chmod 0660 /sys/class/android_usb/android0/enable
    chown system system /sys/class/android_usb/android0/idVendor
    chmod 0660 /sys/class/android_usb/android0/idVendor
    chown system system /sys/class/android_usb/android0/idProduct
    chmod 0660 /sys/class/android_usb/android0/idProduct
    chown system system /sys/class/android_usb/android0/f_diag/clients
    chmod 0660 /sys/class/android_usb/android0/f_diag/clients
    chown system system /sys/class/android_usb/android0/functions
    chmod 0660 /sys/class/android_usb/android0/functions
    chown system system /sys/class/android_usb/android0/bDeviceClass
    chmod 0660 /sys/class/android_usb/android0/bDeviceClass

# Permission for UART SWITCH
    chmod 0660 /sys/class/sec/switch/uart_sel
    chown system system /sys/class/sec/switch/uart_sel

# Permission for RTC
    chmod 0660 /sys/class/rtc/rtc0/hctosys
    chown system system /sys/class/rtc/rtc0/hctosys

on nonencrypted
    mkdir /data/misc/mcRegistry 0775 system drmrpc

on property:vold.decrypt=trigger_restart_framework
    mkdir /data/misc/mcRegistry 0775 system drmrpc

on early-fs
    start vold

on late-fs
    wait_for_prop hwservicemanager.ready true
    wait_for_prop vendor.sys.mobicoredaemon.enable true
    exec_start wait_for_keymaster
    mount_all /vendor/etc/fstab.${ro.hardware} --late
    swapon_all /vendor/etc/fstab.${ro.hardware}

on fs
    start hwservicemanager
    mount_all /vendor/etc/fstab.${ro.hardware} --early

    setprop ro.crypto.fuse_sdcard true

    restorecon_recursive /mnt/vendor/efs
    restorecon_recursive /mnt/vendor/persist

    chown radio system /mnt/vendor/efs
    chown radio system /mnt/vendor/persist

    mkdir /mnt/vendor/persist/data/sfs 0700 system system
    mkdir /mnt/vendor/persist/data/tz 0700 system system

# Gatekeeper data
    mkdir /mnt/vendor/persist/gk 0700 system system

    # Enable rmnet data and aggregation
    setprop persist.rmnet.mux enabled
    setprop persist.rmnet.data.enable true
    setprop persist.data.wda.enable true
    setprop persist.data.df.agg.dl_pkt 10
    setprop persist.data.df.agg.dl_size 4096
# modem partition for cbd
    setprop vendor.cbd.partition modem
# Permissions for ds_detect
    chmod 0660 /sys/devices/platform/cpif/sim/ds_detect
    chown system system /sys/devices/platform/cpif/sim/ds_detect

# Permission for cpif_qos control
    chmod 0660 /sys/kernel/cpif_qos/hiprio_uid
    chown root system /sys/kernel/cpif_qos/hiprio_uid

# Permissions for ION
    chmod 0660 /sys/class/ion_cma/ion_video_ext/isolated
    chown system system /sys/class/ion_cma/ion_video_ext/isolated

# Permissions for backlight
    chmod 0660 /sys/class/backlight/pwm-backlight.0/brightness
    chown system system /sys/class/backlight/pwm-backlight.0/brightness
    chmod 0660 /sys/devices/virtual/backlight/panel_0/brightness
    chown system system /sys/devices/virtual/backlight/panel_0/brightness
    chmod 0660 /sys/devices/platform/panel_0/cabc_mode
    chown system system /sys/devices/platform/panel_0/cabc_mode
    chmod 0660 /sys/devices/platform/panel_0/hbm_mode
    chown system system /sys/devices/platform/panel_0/hbm_mode
# Permissions for dqe
    chmod 0660 /sys/class/dqe/dqe/gamma
    chown system system /sys/class/dqe/dqe/gamma
    chmod 0660 /sys/class/dqe/dqe/aps
    chown system system /sys/class/dqe/dqe/aps
    chmod 0660 /sys/class/dqe/dqe/aps_onoff
    chown system system /sys/class/dqe/dqe/aps_onoff
    chmod 0660 /sys/class/dqe/dqe/aps_lux
    chown system system /sys/class/dqe/dqe/aps_lux
    chmod 0660 /sys/class/dqe/dqe/aps_dim_off
    chown system system /sys/class/dqe/dqe/aps_dim_off
    chmod 0660 /sys/class/dqe/dqe/cgc17_idx
    chown system system /sys/class/dqe/dqe/cgc17_idx
    chmod 0660 /sys/class/dqe/dqe/cgc17_enc
    chown system system /sys/class/dqe/dqe/cgc17_enc
    chmod 0660 /sys/class/dqe/dqe/cgc17_dec
    chown system system /sys/class/dqe/dqe/cgc17_dec
    chmod 0660 /sys/class/dqe/dqe/cgc17_con
    chown system system /sys/class/dqe/dqe/cgc17_con
    chmod 0660 /sys/class/dqe/dqe/hsc48_idx
    chown system system /sys/class/dqe/dqe/hsc48_idx
    chmod 0660 /sys/class/dqe/dqe/hsc48_lcg
    chown system system /sys/class/dqe/dqe/hsc48_lcg
    chmod 0660 /sys/class/dqe/dqe/hsc
    chown system system /sys/class/dqe/dqe/hsc
    chmod 0660 /sys/class/dqe/dqe/scl
    chown system system /sys/class/dqe/dqe/scl
    chmod 0660 /sys/class/dqe/dqe/degamma
    chown system system /sys/class/dqe/dqe/degamma
    chmod 0660 /sys/class/dqe/dqe/gamma_matrix
    chown system system /sys/class/dqe/dqe/gamma_matrix
    chmod 0440 /sys/class/dqe/dqe/xml
    chown system system /sys/class/dqe/dqe/xml
    chmod 0440 /sys/class/dqe/dqe/dqe_ver
    chown system system /sys/class/dqe/dqe/dqe_ver

    chmod 0660 /sys/class/dqe/dqe/color_mode
    chown system system /sys/class/dqe/dqe/color_mode

    chmod 0660 /sys/class/dqe/modeset/mode_idx
    chown system system /sys/class/dqe/modeset/mode_idx
    chmod 0660 /sys/class/dqe/modeset/cgc17_idx
    chown system system /sys/class/dqe/modeset/cgc17_idx
    chmod 0660 /sys/class/dqe/modeset/cgc17_enc
    chown system system /sys/class/dqe/modeset/cgc17_enc
    chmod 0660 /sys/class/dqe/modeset/cgc17_dec
    chown system system /sys/class/dqe/modeset/cgc17_dec
    chmod 0660 /sys/class/dqe/modeset/cgc17_con
    chown system system /sys/class/dqe/modeset/cgc17_con
    chmod 0660 /sys/class/dqe/modeset/gamma
    chown system system /sys/class/dqe/modeset/gamma
    chmod 0660 /sys/class/dqe/modeset/degamma
    chown system system /sys/class/dqe/modeset/degamma
    chmod 0660 /sys/class/dqe/modeset/gamma_matrix
    chown system system /sys/class/dqe/modeset/gamma_matrix
    chmod 0660 /sys/class/dqe/modeset/hsc48_idx
    chown system system /sys/class/dqe/modeset/hsc48_idx
    chmod 0660 /sys/class/dqe/modeset/hsc48_lcg
    chown system system /sys/class/dqe/modeset/hsc48_lcg
    chmod 0660 /sys/class/dqe/modeset/hsc
    chown system system /sys/class/dqe/modeset/hsc
    chmod 0660 /sys/class/dqe/modeset/aps
    chown system system /sys/class/dqe/modeset/aps
    chmod 0660 /sys/class/dqe/modeset/scl
    chown system system /sys/class/dqe/modeset/scl

# Permissions for dqe hdr
    chmod 0660 /sys/class/dqe/hdr/mode_idx
    chown system system /sys/class/dqe/hdr/mode_idx
    chmod 0660 /sys/class/dqe/hdr/gamma
    chown system system /sys/class/dqe/hdr/gamma
    chmod 0660 /sys/class/dqe/hdr/aps
    chown system system /sys/class/dqe/hdr/aps
    chmod 0660 /sys/class/dqe/hdr/cgc17_idx
    chown system system /sys/class/dqe/hdr/cgc17_idx
    chmod 0660 /sys/class/dqe/hdr/cgc17_enc
    chown system system /sys/class/dqe/hdr/cgc17_enc
    chmod 0660 /sys/class/dqe/hdr/cgc17_dec
    chown system system /sys/class/dqe/hdr/cgc17_dec
    chmod 0660 /sys/class/dqe/hdr/cgc17_con
    chown system system /sys/class/dqe/hdr/cgc17_con
    chmod 0660 /sys/class/dqe/hdr/hsc48_idx
    chown system system /sys/class/dqe/hdr/hsc48_idx
    chmod 0660 /sys/class/dqe/hdr/hsc48_lcg
    chown system system /sys/class/dqe/hdr/hsc48_lcg
    chmod 0660 /sys/class/dqe/hdr/hsc
    chown system system /sys/class/dqe/hdr/hsc
    chmod 0660 /sys/class/dqe/hdr/degamma
    chown system system /sys/class/dqe/hdr/degamma
    chmod 0660 /sys/class/dqe/hdr/gamma_matrix
    chown system system /sys/class/dqe/hdr/gamma_matrix
    chmod 0660 /sys/class/dqe/hdr/scl
    chown system system /sys/class/dqe/hdr/scl

# Permissions for rtc
    chmod 0660 /sys/class/rtc/rtc0/bootalarm
    chown system system /sys/class/rtc/rtc0/bootalarm

# Permissions for hdr
    chmod 0660 /sys/class/hdr0/hdr0/hdr
    chown system system /sys/class/hdr0/hdr0/hdr
    chmod 0660 /sys/class/hdr1/hdr1/hdr
    chown system system /sys/class/hdr1/hdr1/hdr
    chmod 0660 /sys/class/hdr2/hdr2/hdr
    chown system system /sys/class/hdr2/hdr2/hdr
    chmod 0660 /sys/class/hdr3/hdr3/hdr
    chown system system /sys/class/hdr3/hdr3/hdr
    chmod 0660 /sys/class/hdr4/hdr4/hdr
    chown system system /sys/class/hdr4/hdr4/hdr
    chmod 0660 /sys/class/hdr5/hdr5/hdr
    chown system system /sys/class/hdr5/hdr5/hdr
    chmod 0660 /sys/class/hdr6/hdr6/hdr
    chown system system /sys/class/hdr6/hdr6/hdr
    chmod 0660 /sys/class/hdr7/hdr7/hdr
    chown system system /sys/class/hdr7/hdr7/hdr

# Permissions for gnss
    chmod 0660 /dev/gnss_ipc
    chown system system /dev/gnss_ipc
# Copy DRM Key
#    copy /system/app/wv.keys /factory/wv.keys

# Permission for DRM Key
#    chmod 0644 /factory/wv.keys

# FM Radio
    chmod 0666 /dev/radio0

# Permision for xlat control
    chmod 0660 /sys/kernel/clat/xlat_plat
    chown clat clat /sys/kernel/clat/xlat_plat
    chmod 0660 /sys/kernel/clat/xlat_addrs
    chown clat clat /sys/kernel/clat/xlat_addrs
    chmod 0660 /sys/kernel/clat/xlat_v4_addrs
    chown clat clat /sys/kernel/clat/xlat_v4_addrs

# Permission for flashlight control for HAL3.3
# The Istor espresso board does not have the flash led h/w, So the below permission line are blocked.
# If you want to test the flashlight in board which have the flash led h/w, Enable the below blocked lines.
    chmod 0660 /sys/class/camera/flash/rear_flash
    chown system camera /sys/class/camera/flash/rear_flash

# Flashlight control for HAL3
    chmod 0660 /sys/class/leds/fled-s2mu106/fled_mode
    chown system camera /sys/class/leds/fled-s2mu106/fled_mode
    chmod 0660 /sys/class/leds/fled-s2mu106/fled_torch_curr
    chown system camera /sys/class/leds/fled-s2mu106/fled_torch_curr

#load ecd firmware
    write /proc/ecd/load_firmware 1

service fuse_sdcard /system/bin/sdcard -u 1023 -g 1023 -d /mnt/media_rw/sdcard /storage/sdcard
    class late_start
    disabled
service fuse_usb1 /system/bin/sdcard -u 1023 -g 1023 -d /mnt/media_rw/usb1 /storage/usb1
    class late_start
    disabled
service fuse_usb2 /system/bin/sdcard -u 1023 -g 1023 -d /mnt/media_rw/usb2 /storage/usb2
    class late_start
    disabled

# epic daemon
    service epicd /vendor/bin/epic
    socket epic dgram 666 system system u:object_r:epicd_socket:s0
    class main
    user system
    group system readproc
    writepid /dev/cpuset/system-background/tasks
    seclabel u:r:epicd:s0
    capabilities SYS_NICE

service abox /vendor/bin/main_abox 16850000.abox /data/vendor/log/abox
    class late_start
    user audioserver
    group system

# CP boot daemon
service cpboot-daemon /vendor/bin/cbd -d -t modap_sit -s 2 -P by-name/${vendor.cbd.partition}
    class main
    user root
    group radio cache inet misc audio sdcard_rw log system
    seclabel u:r:cbd:s0
on property:ro.boot.slot_suffix=*
    setprop vendor.cbd.partition modem${ro.boot.slot_suffix}

# SCsC daemon
service wlbtd /vendor/bin/wlbtd
    class main
    user wifi
    group wifi inet log sdcard_rw misc
# Set watchdog timer to 30 seconds and pet it every 10 seconds to get a 30 second margin
service watchdogd /system/bin/watchdogd 10 30
    class core
    oneshot
    seclabel u:r:watchdogd:s0

# GPS daemon
service gpsd /vendor/bin/hw/gpsd
    class main
    user gps
    group system inet net_raw wakelock
    capabilities BLOCK_SUSPEND
    ioprio be 0
    seclabel u:r:gpsd:s0

# shutdown animation
service shutdownanim /system/bin/bootanimation -shutdown
    class core animation
    user graphics
    group graphics audio
    disabled
    oneshot

# on userdebug and eng builds, enable kgdb on the serial console
on property:ro.debuggable=1
    write /sys/module/kgdboc/parameters/kgdboc ttyFIQ1
    write /sys/module/fiq_debugger/parameters/kgdb_enable 1

## SCSC WLAN
# WiFi wpa suppplicant service
service wpa_supplicant /vendor/bin/hw/wpa_supplicant -ddd -t \
-ip2p0  -Dnl80211 -c/data/vendor/wifi/wpa/p2p_supplicant.conf -N \
-iwlan0 -Dnl80211 -c/data/vendor/wifi/wpa/wpa_supplicant.conf \
-O/data/vendor/wifi/wpa/sockets \
-e/data/misc/wifi/entropy.bin -g@android:wpa_wlan0
##   we will start as root and wpa_supplicant will switch to user wifi
##   after setting up the capabilities required for WEXT
##   user wifi
##   group wifi inet keystore
interface aidl android.hardware.wifi.supplicant.ISupplicant/default

class main
socket wpa_wlan0 dgram 660 wifi wifi
disabled
oneshot
seclabel u:r:hal_wifi_supplicant_default:s0

service dhcpcd_wlan0 /vendor/bin/dhcpcd -aABDKL
        class main
        disabled
        oneshot

service iprenew_wlan0 /vendor/bin/dhcpcd -n
        class main
        disabled
        oneshot

service dhcpcd_p2p /vendor/bin/dhcpcd -aABKL
        class main
        disabled
        oneshot

service iprenew_p2p /vendor/bin/dhcpcd -n
        class main
        disabled
        oneshot

# SPE-1748: PANU DHCP Client not launched
service dhcpcd_bt-pan /vendor/bin/dhcpcd -ABKL
        class main
        disabled
        oneshot

service iprenew_bt-pan /vendor/bin/dhcpcd -n
        class main
        disabled
        oneshot

# IMS WiFi Calling [START] #
service charonservice /system/vendor/bin/charon
        class main
        user root
        group system
        disabled
        seclabel u:r:charonservice:s0

on property:vendor.charon.exec=1
    rm /data/vendor/misc/vpn/charon.pid
    chmod 0666 /dev/tun
    start charonservice

on property:vendor.charon.exec=0
    stop charonservice
    rm /data/vendor/misc/vpn/charon.pid
# IMS WiFi Calling [END] #

# IMS packet router daemon
service pktrouter /system/vendor/bin/wfc-pkt-router
    class main
    user root
    disabled
    seclabel u:r:pktrouter:s0

on property:vendor.pktrouter=1
    start pktrouter

on property:vendor.pktrouter=0
    stop pktrouter

service exynos-thermald /vendor/bin/exynos-thermald
	class late_start
	class main
	user system
	group system
#	oneshot
	file /sys/class/thermal/thermal_zone0 "r"
	seclabel u:r:exynos-thermald:s0

service charger /system/bin/charger -c
	class charger
	group log
	seclabel u:r:charger:s0
