#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

PRODUCT_SHIPPING_API_LEVEL := 30
BOARD_SHIPPING_API_LEVEL   := 30

include $(LOCAL_PATH)/BoardConfig.mk

ifneq ($(filter R%, $(TARGET_BOARD_SUPPORT_FEATURE)),)
BOARD_BOOT_HEADER_VERSION := 3
BOARD_GKI_NONAB_PATH := first_stage_ramdisk
PRODUCT_USE_DYNAMIC_PARTITIONS := true
PRODUCT_USE_VIRTUAL_AB := false
BOARD_VENDOR_BOOTIMAGE_PARTITION_SIZE := 0x04000000
PREBUILD_PATH_BASE := vendor/samsung_slsi/s5e9815/prebuilts/erd9815
CONF_PATH_BASE := device/samsung/erd9815/conf_r
FSTAB_PATH_BASE := device/samsung/erd9815/conf_r
ifeq ($(PRODUCT_USE_VIRTUAL_AB),true)
AB_OTA_UPDATER := true
AB_OTA_PARTITIONS := \
	system \
	vendor \
	vbmeta
TARGET_NO_RECOVERY := true
BOARD_USES_RECOVERY_AS_BOOT := true
FSTAB_PATH_BASE := device/samsung/erd9815/conf_r
else
FSTAB_PATH_BASE := device/samsung/erd9815/conf_r/non_ab
endif
BOARD_KERNEL_MOUDLES := true
BOARD_USES_SW_GATEKEEPER := false
USE_SWIFTSHADER := false
endif

BOARD_PREBUILTS := device/samsung/$(TARGET_PRODUCT:full_%=%)-prebuilts

ifeq ($(wildcard $(BOARD_PREBUILTS)),)
INSTALLED_KERNEL_TARGET := $(PREBUILD_PATH_BASE)/kernel
BOARD_PREBUILT_DTBOIMAGE := $(PREBUILD_PATH_BASE)/dtbo.img
BOARD_PREBUILT_DTB := $(PREBUILD_PATH_BASE)/erd9815.dtb
BOARD_PREBUILT_BOOTLOADER_IMG := $(PREBUILD_PATH_BASE)/bootloader.img
INSTALLED_DTBIMAGE_TARGET := $(BOARD_PREBUILT_DTB)
else
PREBUILD_PATH_BASE := $(BOARD_PREBUILTS)
INSTALLED_KERNEL_TARGET := $(BOARD_PREBUILTS)/kernel
BOARD_PREBUILT_DTBOIMAGE := $(BOARD_PREBUILTS)/dtbo.img
BOARD_PREBUILT_DTB := $(BOARD_PREBUILTS)/dtb.img
BOARD_PREBUILT_BOOTLOADER_IMG := $(BOARD_PREBUILTS)/bootloader.img
INSTALLED_DTBIMAGE_TARGET := $(BOARD_PREBUILT_DTB)
endif

ifeq ($(BOARD_KERNEL_MOUDLES),true)
BOARD_PREBUILT_KERNEL_MODULES := $(PREBUILD_PATH_BASE)/modules
BOARD_PREBUILT_KERNEL_MODULES_SYMBOLS := $(PREBUILD_PATH_BASE)/modules_symbols
ifneq ($(wildcard $(PREBUILD_PATH_BASE)/vendor_module_list.cfg),)
PRODUCT_COPY_FILES += $(PREBUILD_PATH_BASE)/vendor_module_list.cfg:$(TARGET_COPY_OUT_VENDOR)/etc/init.insmod.$(TARGET_SOC).cfg
VENDOR_KERNEL_MODULE_MODPROBE_LIST := $(shell cat $(PREBUILD_PATH_BASE)/vendor_module_list.cfg | grep -v '#' | grep 'modprobe|' | sed 's/modprobe|//g')
VENDOR_KERNEL_MODULE_INSMOD_LIST := $(shell cat $(PREBUILD_PATH_BASE)/vendor_module_list.cfg | grep -v '#' | grep 'insmod|' | sed 's/insmod|//g')
VENDOR_KERNEL_MODULE_LIST += $(foreach modules, $(VENDOR_KERNEL_MODULE_MODPROBE_LIST) $(VENDOR_KERNEL_MODULE_INSMOD_LIST),$(notdir $(modules)))
endif
ifneq ($(wildcard $(PREBUILD_PATH_BASE)/vendor_boot_module_order.cfg),)
PRODUCT_COPY_FILES += $(PREBUILD_PATH_BASE)/vendor_boot_module_order.cfg:$(TARGET_COPY_OUT_VENDOR)/etc/init.reorder.$(TARGET_SOC).cfg
VENDOR_REORDER_LIST := $(shell cat $(PREBUILD_PATH_BASE)/vendor_boot_module_order.cfg | grep -v "#")
endif
endif

ifeq ($(wildcard $(BOARD_PREBUILTS)),)
PRODUCT_COPY_FILES += \
		$(INSTALLED_KERNEL_TARGET):$(PRODUCT_OUT)/kernel \
		device/samsung/erd9815/flashall.sh:$(PRODUCT_OUT)/flashall.sh \
		device/samsung/erd9815/flashall.bat:$(PRODUCT_OUT)/flashall.bat
else
PRODUCT_COPY_FILES += $(foreach image,\
	$(filter-out $(BOARD_PREBUILT_DTBOIMAGE) $(BOARD_PREBUILT_BOOTLOADER_IMG) $(BOARD_PREBUILT_DTB) $(BOARD_PREBUILT_KERNEL_MODULES) $(BOARD_PREBUILT_KERNEL_MODULES_SYMBOLS), $(wildcard $(BOARD_PREBUILTS)/*)),\
	$(image):$(PRODUCT_OUT)/$(notdir $(image)))
endif

ifeq ($(BOARD_KERNEL_MOUDLES),true)
# Reordering modules list
BOARD_VENDOR_RAMDISK_KERNEL_MODULES += $(foreach module,\
$(VENDOR_REORDER_LIST),\
$(if $(wildcard $(BOARD_PREBUILT_KERNEL_MODULES)/$(module)),$(BOARD_PREBUILT_KERNEL_MODULES)/$(module),\
$(warning cannot find $(module), SKIPPING...)))
PRODUCT_COPY_FILES += device/samsung/erd9815/conf/init.insmod.sh:/vendor/bin/init.insmod.sh
PRODUCT_COPY_FILES += $(foreach modules, $(wildcard $(BOARD_PREBUILT_KERNEL_MODULES_SYMBOLS)/*), $(modules):$(PRODUCT_OUT)/modules_symbols/$(notdir $(modules)))
BOARD_VENDOR_RAMDISK_KERNEL_MODULES += $(foreach module, $(filter-out $(VENDOR_KERNEL_MODULE_LIST) $(VENDOR_REORDER_LIST), $(notdir $(shell cat $(BOARD_PREBUILT_KERNEL_MODULES)/modules.order))),$(BOARD_PREBUILT_KERNEL_MODULES)/$(module))
BOARD_VENDOR_KERNEL_MODULES += $(foreach module, $(VENDOR_KERNEL_MODULE_MODPROBE_LIST), $(BOARD_PREBUILT_KERNEL_MODULES)/$(notdir $(module)))
ifneq ($(VENDOR_KERNEL_MODULE_INSMOD_LIST),)
PRODUCT_COPY_FILES += $(foreach modules, $(VENDOR_KERNEL_MODULE_INSMOD_LIST), $(BOARD_PREBUILT_KERNEL_MODULES)/$(notdir $(modules)):$(modules))
endif
ifneq ($(BOARD_USES_RECOVERY_AS_BOOT),true)
BOARD_RECOVERY_KERNEL_MODULES += $(BOARD_VENDOR_RAMDISK_KERNEL_MODULES)
endif
endif

ifeq ($(PRODUCT_USE_VIRTUAL_AB),true)
EXYNOS_BOOT_CONTROL_NAME := android.hardware.boot@1.1-impl-exynos
PRODUCT_PACKAGES += \
	$(EXYNOS_BOOT_CONTROL_HAL) \
	$(EXYNOS_BOOT_CONTROL_HAL).recovery \
	bootctrl.s5e9815 \
	update_engine \
	update_verifier
endif

# recovery mode
ifneq ($(PRODUCT_USE_VIRTUAL_AB),true)
BOARD_INCLUDE_RECOVERY_DTBO := true
endif

ifneq ($(call math_gt_or_eq,$(BOARD_BOOT_HEADER_VERSION),3),)
BOARD_INCLUDE_DTB_IN_BOOTIMG := true
BOARD_IMAGE_BASE_ADDRESS := 0x80000000
BOARD_KERNEL_OFFSET := 0x00080000
BOARD_RAMDISK_OFFSET := 0x04000000
BOARD_DTB_OFFSET := 0x0A000000
BOARD_KERNEL_TAGS_OFFSET := 0

ifneq ($(BOARD_USES_RECOVERY_AS_BOOT),true)
BOARD_RECOVERY_MKBOOTIMG_ARGS := \
  --base $(BOARD_IMAGE_BASE_ADDRESS) \
  --kernel_offset $(BOARD_KERNEL_OFFSET) \
  --ramdisk_offset $(BOARD_RAMDISK_OFFSET) \
  --tags_offset $(BOARD_KERNEL_TAGS_OFFSET) \
  --header_version 2 \
  --dtb $(INSTALLED_DTBIMAGE_TARGET) \
  --dtb_offset $(BOARD_DTB_OFFSET) \
  --pagesize $(BOARD_FLASH_BLOCK_SIZE) \
  --recovery_dtbo $(BOARD_PREBUILT_DTBOIMAGE)
endif

BOARD_MKBOOTIMG_ARGS := \
  --base $(BOARD_IMAGE_BASE_ADDRESS) \
  --kernel_offset $(BOARD_KERNEL_OFFSET) \
  --ramdisk_offset $(BOARD_RAMDISK_OFFSET) \
  --tags_offset $(BOARD_KERNEL_TAGS_OFFSET) \
  --header_version $(BOARD_BOOT_HEADER_VERSION) \
  --dtb $(INSTALLED_DTBIMAGE_TARGET) \
  --dtb_offset $(BOARD_DTB_OFFSET) \
  --pagesize $(BOARD_FLASH_BLOCK_SIZE)
else
BOARD_RAMDISK_OFFSET := 0
BOARD_KERNEL_TAGS_OFFSET := 0
BOARD_MKBOOTIMG_ARGS := \
  --ramdisk_offset $(BOARD_RAMDISK_OFFSET) \
  --tags_offset $(BOARD_KERNEL_TAGS_OFFSET) \
  --header_version $(BOARD_BOOT_HEADER_VERSION)
endif



PRODUCT_COPY_FILES += \
		$(INSTALLED_KERNEL_TARGET):$(PRODUCT_OUT)/kernel


# Partitions options
ifneq ($(PRODUCT_USE_VIRTUAL_AB),true)
BOARD_BOOTIMAGE_PARTITION_SIZE := 0x04000000
else
BOARD_BOOTIMAGE_PARTITION_SIZE := 0x04000000
endif
BOARD_DTBOIMG_PARTITION_SIZE := 0x00100000

BOARD_VENDORIMAGE_FILE_SYSTEM_TYPE := ext4
BOARD_VENDORIMAGE_PARTITION_SIZE := 734003200

TARGET_USERIMAGES_USE_EXT4 := true
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 4026531840
BOARD_USERDATAIMAGE_PARTITION_SIZE := 11796480000
BOARD_MOUNT_SDCARD_RW := true

ifneq ($(PRODUCT_USE_VIRTUAL_AB),true)
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 0x08000000
BOARD_CACHEIMAGE_PARTITION_SIZE := 69206016
BOARD_CACHEIMAGE_FILE_SYSTEM_TYPE := ext4
endif

ifeq ($(PRODUCT_USE_DYNAMIC_PARTITIONS),true)
# Dynamic Partitions options
ifneq ($(PRODUCT_USE_VIRTUAL_AB),true)
BOARD_SUPER_PARTITION_SIZE := 5242880000
else
BOARD_SUPER_PARTITION_SIZE := 6287261696
endif

# Configuration for dynamic partitions
BOARD_SUPER_PARTITION_GROUPS := group_basic
BOARD_GROUP_BASIC_SIZE := 5238685696
ifeq ($(WITH_ESSI),true)
BOARD_GROUP_BASIC_PARTITION_LIST := vendor
else
BOARD_GROUP_BASIC_PARTITION_LIST := system vendor
endif
endif


# From system.property
PRODUCT_PROPERTY_OVERRIDES += \
    persist.demo.hdmirotationlock=false \
    dev.usbsetting.embedded=on \
    audio.offload.min.duration.secs=30

# From system.property
PRODUCT_PROPERTY_OVERRIDES += \
	ro.incremental.enable=module:/vendor/lib/modules/incrementalfs.ko

# Device Manifest, Device Compatibility Matrix for Treble
ifeq ($(PRODUCT_USE_VIRTUAL_AB),true)
ifeq ($(BOARD_USES_EXYNOS_GRALLOC_VERSION),4)
DEVICE_MANIFEST_FILE := \
	device/samsung/erd9815/manifest_abupdate.xml
else
DEVICE_MANIFEST_FILE := \
	device/samsung/erd9815/manifest_abupdate_legacy_gralloc.xml
endif # BOARD_USES_EXYNOS_GRALLOC_VERSION
else
ifeq ($(BOARD_USES_EXYNOS_GRALLOC_VERSION),4)
DEVICE_MANIFEST_FILE := \
	device/samsung/erd9815/manifest.xml
else
DEVICE_MANIFEST_FILE := \
	device/samsung/erd9815/manifest_legacy_gralloc.xml
endif # BOARD_USES_EXYNOS_GRALLOC_VERSION
endif # PRODUCT_USE_VIRTUAL_AB)

DEVICE_MATRIX_FILE := \
	device/samsung/erd9815/compatibility_matrix.xml

DEVICE_FRAMEWORK_COMPATIBILITY_MATRIX_FILE := \
	device/samsung/erd9815/framework_compatibility_matrix.xml


# These are for the multi-storage mount.
#ifeq ($(BOARD_USES_BOOT_STORAGE),false)
#DEVICE_PACKAGE_OVERLAYS := \
	device/samsung/erd9815/overlay-ufsboot
#else ifeq ($(BOARD_USES_BOOT_STORAGE), sdboot)
#	DEVICE_PACKAGE_OVERLAYS := \
		device/samsung/erd9815/overlay-sdboot
#else ifeq ($(BOARD_USES_BOOT_STORAGE), emmc)
DEVICE_PACKAGE_OVERLAYS := \
	device/samsung/erd9815/overlay-ufsboot
#endif
DEVICE_PACKAGE_OVERLAYS += device/samsung/erd9815/overlay

# Init files
PRODUCT_COPY_FILES += \
	$(CONF_PATH_BASE)/init.s5e9815.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.$(TARGET_SOC).rc \
	$(CONF_PATH_BASE)/init.s5e9815.usb.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.$(TARGET_SOC).usb.rc \
	device/samsung/erd9815/task_profiles.json:$(TARGET_COPY_OUT_VENDOR)/etc/task_profiles.json \
	device/samsung/erd9815/conf/ueventd.s5e9815.rc:$(TARGET_COPY_OUT_VENDOR)/ueventd.rc \
	device/samsung/erd9815/conf/init.recovery.s5e9815.rc:root/init.recovery.$(TARGET_SOC).rc
#fstab file selection:
# It must be : fstab.{device_name erd9815}.{storage_option ufs}.{if dynamic_partition dp}
ifeq ($(PRODUCT_USE_DYNAMIC_PARTITIONS), true)
BOOTUP_STORAGE := $(BOARD_USES_BOOT_STORAGE).dp
else
BOOTUP_STORAGE := $(BOARD_USES_BOOT_STORAGE)
endif
PRODUCT_COPY_FILES += \
	$(FSTAB_PATH_BASE)/fstab.s5e9815.$(BOOTUP_STORAGE):$(TARGET_COPY_OUT_VENDOR)/etc/fstab.$(TARGET_SOC) \
	$(FSTAB_PATH_BASE)/fstab.s5e9815.$(BOOTUP_STORAGE):$(TARGET_COPY_OUT_RAMDISK)/fstab.$(TARGET_SOC)
#ifeq ($(BOARD_USES_RECOVERY_AS_BOOT),true)
PRODUCT_COPY_FILES += \
	$(FSTAB_PATH_BASE)/fstab.s5e9815.$(BOOTUP_STORAGE):$(TARGET_COPY_OUT_VENDOR_RAMDISK)/fstab.$(TARGET_SOC) \
	$(FSTAB_PATH_BASE)/fstab.s5e9815.$(BOOTUP_STORAGE):$(TARGET_COPY_OUT_VENDOR_RAMDISK)/first_stage_ramdisk/fstab.$(TARGET_SOC)

#endif
TARGET_RECOVERY_FSTAB := $(FSTAB_PATH_BASE)/fstab.s5e9815.$(BOOTUP_STORAGE)


# Add Recovery boot rc and fstab for GKI recovery booting
PRODUCT_COPY_FILES += \
	device/samsung/erd9815/conf/init.recovery.s5e9815.rc:$(TARGET_COPY_OUT_VENDOR_RAMDISK)/init.recovery.$(TARGET_SOC).rc \
	$(CONF_PATH_BASE)/init.s5e9815.usb.rc:$(TARGET_COPY_OUT_VENDOR_RAMDISK)/etc/init/init.$(TARGET_SOC).usb.rc \
	$(FSTAB_PATH_BASE)/fstab.s5e9815.$(BOOTUP_STORAGE):$(TARGET_COPY_OUT_VENDOR_RAMDISK)/etc/recovery.fstab

# Support devtools
ifeq ($(TARGET_BUILD_VARIANT),eng)
PRODUCT_PACKAGES += \
	Development
endif

# Filesystem management tools
PRODUCT_PACKAGES += \
	e2fsck

# RPMB TA
PRODUCT_PACKAGES += \
	tlrpmb \
	tlrpmb_64

# RPMB test application (only for eng build)
ifneq (,$(filter eng, $(TARGET_BUILD_VARIANT)))
PRODUCT_PACKAGES += \
	bRPMB \
	bRPMB-CMD
endif

# color mode
PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
    persist.sys.sf.color_saturation=1.0

# boot animation files
PRODUCT_COPY_FILES += \
    device/samsung/erd9815/bootanim/bootanimation_part1.zip:system/media/bootanimation.zip \
    device/samsung/erd9815/bootanim/shutdownanimation.zip:system/media/shutdownanimation.zip

# dqe calib xml
PRODUCT_COPY_FILES += \
    device/samsung/erd9815/dqe/calib_data_colortemp.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/calib_data_colortemp.xml \
    device/samsung/erd9815/dqe/calib_data_eyetemp.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/calib_data_eyetemp.xml \
    device/samsung/erd9815/dqe/calib_data_rgbgain.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/calib_data_rgbgain.xml \
    device/samsung/erd9815/dqe/calib_data_skincolor.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/calib_data_skincolor.xml \
    device/samsung/erd9815/dqe/calib_data_whitepoint.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/calib_data_whitepoint.xml \
    device/samsung/erd9815/dqe/calib_data_bypass.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/calib_data_bypass.xml \
    device/samsung/erd9815/dqe/DQE_coef_data_s6e3ha8_fhdp_ddi.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/DQE_coef_data_s6e3ha8_fhdp_ddi.xml \
    device/samsung/erd9815/dqe/HDR_coef_data_s6e3ha8_fhdp_ddi_HDR.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/HDR_coef_data_s6e3ha8_fhdp_ddi_HDR.xml \
    device/samsung/erd9815/dqe/HDR_coef_data_s6e3ha8_fhdp_ddi_HDR10P.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/HDR_coef_data_s6e3ha8_fhdp_ddi_HDR10P.xml

# AVB2.0, to tell PackageManager that the system supports Verified Boot(PackageManager.FEATURE_VERIFIED_BOOT)
ifeq ($(BOARD_AVB_ENABLE), true)
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.software.verified_boot.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.software.verified_boot.xml
endif

ifeq ($(BOARD_USES_EXYNOS_SENSORS_HAL),true)
# Enable support for chinook sensorhu
TARGET_USES_CHINOOK_SENSORHUB := false

PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.sensor.gyroscope.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.gyroscope.xml\
    frameworks/native/data/etc/android.hardware.sensor.accelerometer.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.accelerometer.xml \
    frameworks/native/data/etc/android.hardware.sensor.light.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.light.xml\
    frameworks/native/data/etc/android.hardware.sensor.barometer.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.barometer.xml \
    frameworks/native/data/etc/android.hardware.sensor.proximity.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.proximity.xml \
    frameworks/native/data/etc/android.hardware.sensor.compass.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.compass.xml \
    frameworks/native/data/etc/android.hardware.sensor.stepcounter.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.stepcounter.xml \
    frameworks/native/data/etc/android.hardware.sensor.stepdetector.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.stepdetector.xml \
    device/samsung/erd9815/init.exynos.nanohub.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.exynos.sensorhub.rc

# Copy CHUB os binary file
ifneq ("$(wildcard device/samsung/erd9815/firmware/os.checked*.bin)", "")
PRODUCT_COPY_FILES += $(foreach image,\
    $(wildcard device/samsung/erd9815/firmware/os.checked*.bin),\
    $(image):$(TARGET_COPY_OUT_VENDOR)/firmware/$(notdir $(image)))
else
PRODUCT_COPY_FILES += \
    device/samsung/erd9815/firmware/chub_nanohub_0.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/os.checked_0.bin \
    device/samsung/erd9815/firmware/chub_nanohub_1.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/os.checked_1.bin
endif

# Copy CHUB bl binary file
ifneq ("$(wildcard device/samsung/erd9815/firmware/bl.unchecked.bin)", "")
PRODUCT_COPY_FILES += \
    device/samsung/erd9815/firmware/bl.unchecked.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/bl.unchecked.bin
else
PRODUCT_COPY_FILES += \
    device/samsung/erd9815/firmware/chub_bl.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/bl.unchecked.bin
endif

# Copy CHUB os elf file
ifneq ("$(wildcard device/samsung/erd9815/firmware/os.checked*.elf)", "")
PRODUCT_COPY_FILES += $(foreach image,\
    $(wildcard device/samsung/erd9815/firmware/os.checked*.elf),\
    $(image):$(TARGET_COPY_OUT_VENDOR)/firmware/$(notdir $(image)))
endif

# Copy CHUB bl elf file
ifneq ("$(wildcard device/samsung/erd9815/firmware/bl.unchecked.elf)", "")
PRODUCT_COPY_FILES += \
    device/samsung/erd9815/firmware/bl.unchecked.elf:$(TARGET_COPY_OUT_VENDOR)/firmware/bl.unchecked.elf
endif

# Sensor HAL
NANOHUB_SENSORHAL_SENSORLIST := $(LOCAL_PATH)/sensorhal/sensorlist.cpp
NANOHUB_SENSORHAL_EXYNOS_CONTEXTHUB := true
NANOHUB_SENSORHAL_NAME_OVERRIDE := sensors.$(TARGET_BOARD_PLATFORM)

# DPU test HAL
#PRODUCT_PACKAGES += dpu_test

ifeq ($(NANOHUB_SENSORHAL2), true)
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/sensors.hals.conf:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/hals.conf

PRODUCT_PACKAGES += \
    $(NANOHUB_SENSORHAL_NAME_OVERRIDE) \
    android.hardware.sensors@2.0-service.multihal \
    android.hardware.sensors@2.0-service.multihal.rc \
    android.hardware.sensors@2.0-exynossubhal
else
TARGET_USES_NANOHUB_SENSORHAL := true
PRODUCT_PACKAGES += \
    $(NANOHUB_SENSORHAL_NAME_OVERRIDE) \
    context_hub.default \
    android.hardware.sensors@1.0-impl \
    android.hardware.sensors@1.0-service
endif

# sensor utilities (only for userdebug and eng builds)
ifneq (,$(filter userdebug eng, $(TARGET_BUILD_VARIANT)))
PRODUCT_PACKAGES += \
    samsung_nanotool \
    nanoapp_cmd \
    sensortest
endif

ifeq ($(NANOHUB_TESTSUITE), true)
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/sensorhal/chub_test_pr.sh:$(TARGET_COPY_OUT_VENDOR)/bin/chub_test_pr.sh \
    $(LOCAL_PATH)/sensorhal/chub_test_reboot.sh:$(TARGET_COPY_OUT_VENDOR)/bin/chub_test_reboot.sh

PRODUCT_PACKAGES += \
    chub_test \
    sensortest_rand
endif
endif

ifeq ($(BOARD_USES_EXYNOS_SENSORS_DUMMY), true)
# Sensor HAL
PRODUCT_PACKAGES += \
        android.hardware.sensors@1.0-impl \
        android.hardware.sensors@1.0-service \
        sensors.$(TARGET_SOC)
endif

# MEMLOGGER
PRODUCT_PACKAGES += \
	mlgcat

PRODUCT_PACKAGES += \
	memlogd \
	libmemlogservice \
	vendor.samsung_slsi.memlogger.hardware.memlogservice@1.0 \
	MemlogController

# USB HAL
PRODUCT_PACKAGES += \
	android.hardware.usb@1.1 \
	android.hardware.usb@1.1-service

ifeq ($(PRODUCT_USE_DYNAMIC_PARTITIONS),true)
# Fastboot HAL
PRODUCT_PACKAGES += \
       fastbootd \
       android.hardware.fastboot@1.1\
       android.hardware.fastboot@1.1-impl-mock.s5e9815
endif

# Power HAL
PRODUCT_SOONG_NAMESPACES += device/samsung/erd9815/power

PRODUCT_PACKAGES += \
	android.hardware.power-service.exynos.s5e9815

# exynosdisplayfeature
PRODUCT_PACKAGES += \
	vendor.samsung_slsi.hardware.exynosdisplayfeature@1.0 \
	vendor.samsung_slsi.hardware.exynosdisplayfeature@1.0-impl \
	vendor.samsung_slsi.hardware.exynosdisplayfeature@1.0-service \
	exynosdisplayfeature_client

# Thermal HAL
PRODUCT_PACKAGES += \
	android.hardware.thermal@2.0-impl \
	android.hardware.thermal@2.0-service.exynos

#Health 1.0 HAL
PRODUCT_PACKAGES += \
	android.hardware.health@2.1-impl \
	android.hardware.health@2.1-impl.recovery \
	android.hardware.health@2.1-service

#
# Audio HALs
#
PRODUCT_SOONG_NAMESPACES += device/samsung/erd9815/audio
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/modules/libaudio/audiohal_comv2_vv/odm_specific/sitril

# Audio Configurations
USE_LEGACY_LOCAL_AUDIO_HAL := true
USE_XML_AUDIO_POLICY_CONF := 1

# Audio HAL Server & Default Implementations
PRODUCT_PACKAGES += \
    android.hardware.audio.service \
    android.hardware.audio@7.0-impl \
    android.hardware.audio.effect@7.0-impl \
    android.hardware.soundtrigger@2.3-impl

# AudioHAL libraries
PRODUCT_PACKAGES += \
	audio.primary.$(TARGET_SOC) \
	audio.usb.default \
	audio.r_submix.default \
	audio.bluetooth.default

# AudioHAL Configurations
PRODUCT_COPY_FILES += \
	device/samsung/erd9815/audio/config/audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_policy_configuration.xml \
	device/samsung/erd9815/audio/config/usb_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/usb_audio_policy_configuration.xml \
	device/samsung/erd9815/audio/config/bluetooth_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/bluetooth_audio_policy_configuration.xml \
        frameworks/av/services/audiopolicy/config/a2dp_in_audio_policy_configuration_7_0.xml:$(TARGET_COPY_OUT_VENDOR)/etc/a2dp_in_audio_policy_configuration_7_0.xml \
	frameworks/av/services/audiopolicy/config/r_submix_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/r_submix_audio_policy_configuration.xml \
	frameworks/av/services/audiopolicy/config/audio_policy_volumes.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_policy_volumes.xml \
	frameworks/av/services/audiopolicy/config/default_volume_tables.xml:$(TARGET_COPY_OUT_VENDOR)/etc/default_volume_tables.xml \

# Mixer Path Configuration for AudioHAL
PRODUCT_COPY_FILES += \
	device/samsung/erd9815/audio/config/mixer_paths.xml:$(TARGET_COPY_OUT_VENDOR)/etc/mixer_paths.xml \
	device/samsung/erd9815/audio/config/audio_board_info.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_board_info.xml

# AudioEffectHAL Configuration
PRODUCT_COPY_FILES += \
	device/samsung/erd9815/audio/config/audio_effects.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_effects.xml

# MIDI support native XML
PRODUCT_COPY_FILES += \
        frameworks/native/data/etc/android.software.midi.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.software.midi.xml

# Enable AAudio MMAP/NOIRQ data path.
PRODUCT_PROPERTY_OVERRIDES += aaudio.mmap_policy=2
PRODUCT_PROPERTY_OVERRIDES += aaudio.mmap_exclusive_policy=2
PRODUCT_PROPERTY_OVERRIDES += aaudio.hw_burst_min_usec=2000

# Calliope & VTS firmware overwrite
PRODUCT_COPY_FILES += \
	device/samsung/erd9815/firmware/audio/vts.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/vts.bin \
	device/samsung/erd9815/firmware/audio/abox_tplg.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/abox_tplg.bin \
	device/samsung/erd9815/firmware/audio/calliope_dram.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_dram.bin \
	device/samsung/erd9815/firmware/audio/calliope_sram.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_sram.bin \
	device/samsung/erd9815/firmware/audio/calliope_dram_2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_dram_2.bin \
	device/samsung/erd9815/firmware/audio/calliope_sram_2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_sram_2.bin \
	device/samsung/erd9815/firmware/audio/param_aprxse.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/param_aprxse.bin \
	device/samsung/erd9815/firmware/audio/param_aptxse.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/param_aptxse.bin \
	device/samsung/erd9815/firmware/audio/param_cprxse.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/param_cprxse.bin \
	device/samsung/erd9815/firmware/audio/param_cptxse.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/param_cptxse.bin \
	device/samsung/erd9815/firmware/audio/rxse.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/rxse.bin \
	device/samsung/erd9815/firmware/audio/txse.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/txse.bin \
	device/samsung/erd9815/firmware/audio/ASWIP1.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/ASWIP1.bin \
	device/samsung/erd9815/firmware/audio/ASWIP2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/ASWIP2.bin \
	device/samsung/erd9815/firmware/audio/RECSOL.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/RECSOL.bin \
	device/samsung/erd9815/firmware/audio/ASWIP1_param.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/ASWIP1_param.bin \
	device/samsung/erd9815/firmware/audio/ASWIP2_param.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/ASWIP2_param.bin \
	device/samsung/erd9815/firmware/audio/RECSOL_param.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/RECSOL_param.bin \
	device/samsung/erd9815/firmware/audio/EXYNOS981_ABOX_slog.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/orion_slog.bin \
	device/samsung/erd9815/firmware/audio/abox_a2dp.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/abox_a2dp.bin

# AudioEffectHAL library
ifeq ($(BOARD_USE_OFFLOAD_AUDIO), true)
ifeq ($(BOARD_USE_OFFLOAD_EFFECT),true)
PRODUCT_PACKAGES += \
	libexynospostprocbundle
endif
endif

# SoundTriggerHAL library
ifeq ($(BOARD_USE_SOUNDTRIGGER_HAL_VV), true)
PRODUCT_PACKAGES += \
	sound_trigger.primary.$(TARGET_SOC)
# VoiceTriggerSystem (VTS) HW test application
PRODUCT_PACKAGES_ENG += \
        vtshw-test
endif

# Audio Dump Service & Audiologging APK
ifeq ($(BOARD_USES_AUDIO_LOGGING_SERVICE), true)
PRODUCT_PACKAGES += \
    vendor.samsung_slsi.hardware.audio_dump@1.0-service \
    AudioLogging
endif

# control_privapp_permissions
PRODUCT_PROPERTY_OVERRIDES += ro.control_privapp_permissions=enforce

# TinyTools for Audio
ifneq ($(filter eng userdebug,$(TARGET_BUILD_VARIANT)),)
PRODUCT_PACKAGES += \
    tinyplay \
    tinycap \
    tinymix \
    tinypcminfo \
    tinyhostless
endif


# Libs
PRODUCT_PACKAGES += \
	com.android.future.usb.accessory

# Gralloc libs
ifeq ($(BOARD_USES_EXYNOS_GRALLOC_VERSION),4)
PRODUCT_PACKAGES += \
    android.hardware.graphics.mapper@4.0-impl \
    android.hardware.graphics.allocator@4.0-service \
    android.hardware.graphics.allocator@4.0-impl
else
PRODUCT_PACKAGES += \
    android.hardware.graphics.mapper@2.0-impl-2.1 \
    android.hardware.graphics.allocator@2.0-service \
    android.hardware.graphics.allocator@2.0-impl \
    gralloc.$(TARGET_SOC)
endif

PRODUCT_PACKAGES += \
    memtrack.$(TARGET_BOOTLOADER_BOARD_NAME)\
    android.hardware.memtrack@1.0-impl \
    android.hardware.memtrack@1.0-service \
    libion_exynos \
    libion

PRODUCT_PACKAGES += \
    libhwjpeg

# Video Editor
PRODUCT_PACKAGES += \
	VideoEditorGoogle

# WideVine modules
PRODUCT_PACKAGES += \
	android.hardware.drm@latest-service.clearkey \
	android.hardware.drm@latest-service.widevine

# SecureDRM modules
PRODUCT_PACKAGES += \
	tlwvdrm \
	tlsecdrm \
	liboemcrypto_modular

#Mobicore
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos/tee/kinibi510

# MobiCore setup
PRODUCT_PACKAGES += \
	libMcClient \
	libMcRegistry \
	libgdmcprov \
	mcDriverDaemon \
	vendor.trustonic.tee@1.1-service \
	RootPA \
	TeeService \
	libTeeClient \
	libteeservice_client.trustonic \
	tee_tlcm \
	tee_whitelist \
	public.libraries-trustonic.txt

# Camera HAL
PRODUCT_PACKAGES += \
    android.hardware.camera.provider@2.4-impl \
    android.hardware.camera.provider@2.4-service_64 \
    camera.$(TARGET_SOC)

# Copy FIMC_IS DDK Libraries
PRODUCT_COPY_FILES += \
    device/samsung/erd9815/firmware/camera/is_lib.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/is_lib.bin \
    device/samsung/erd9815/firmware/camera/is_rta.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/is_rta.bin \
    device/samsung/erd9815/firmware/camera/setfile_2l4.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_2l4.bin \
    device/samsung/erd9815/firmware/camera/setfile_3m5.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_3m5.bin \
    device/samsung/erd9815/firmware/camera/setfile_3p9.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_3p9.bin \
    device/samsung/erd9815/firmware/camera/setfile_3h1.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_3h1.bin \
    device/samsung/erd9815/firmware/camera/setfile_gw1_v2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_gw1_v2.bin \
    device/samsung/erd9815/firmware/camera/setfile_gd1_tele.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_gd1_tele.bin \
    device/samsung/erd9815/firmware/camera/setfile_3m5fold.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_3m5fold.bin \
    device/samsung/erd9815/firmware/camera/setfile_hi846.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_hi846.bin

# Copy Camera HFD Setfiles
#PRODUCT_COPY_FILES += \
    device/samsung/erd9815/firmware/camera/libhfd/default_configuration.hfd.cfg.json:$(TARGET_COPY_OUT_VENDOR)/firmware/default_configuration.hfd.cfg.json \
    device/samsung/erd9815/firmware/camera/libhfd/pp_cfg.json:$(TARGET_COPY_OUT_VENDOR)/firmware/pp_cfg.json \
    device/samsung/erd9815/firmware/camera/libhfd/tracker_cfg.json:$(TARGET_COPY_OUT_VENDOR)/firmware/tracker_cfg.json \
    device/samsung/erd9815/firmware/camera/libhfd/WithLightFixNoBN.SDNNmodel:$(TARGET_COPY_OUT_VENDOR)/firmware/WithLightFixNoBN.SDNNmodel

PRODUCT_COPY_FILES += \
	device/samsung/erd9815/handheld_core_hardware.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/handheld_core_hardware.xml \
	frameworks/native/data/etc/android.hardware.camera.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.xml \
	frameworks/native/data/etc/android.hardware.camera.front.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.front.xml \
	frameworks/native/data/etc/android.hardware.camera.flash-autofocus.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.flash-autofocus.xml \
	frameworks/native/data/etc/android.hardware.camera.full.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.full.xml \
	frameworks/native/data/etc/android.hardware.camera.raw.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.raw.xml \
	frameworks/native/data/etc/android.hardware.wifi.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.wifi.xml \
	frameworks/native/data/etc/android.hardware.wifi.direct.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.wifi.direct.xml \
	frameworks/native/data/etc/android.hardware.usb.host.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.usb.host.xml \
	frameworks/native/data/etc/android.hardware.usb.accessory.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.usb.accessory.xml \
	frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
	frameworks/native/data/etc/android.hardware.audio.low_latency.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.audio.low_latency.xml

PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.wifi.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.wifi.xml \
	frameworks/native/data/etc/android.hardware.wifi.passpoint.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.wifi.passpoint.xml \
	frameworks/native/data/etc/android.hardware.wifi.direct.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.wifi.direct.xml \
#PRODUCT_COPY_FILES += \
	device/samsung/erd9815/handheld_core_hardware.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/handheld_core_hardware.xml \
	frameworks/native/data/etc/android.hardware.wifi.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.wifi.xml \
	frameworks/native/data/etc/android.hardware.wifi.passpoint.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.wifi.passpoint.xml \
	frameworks/native/data/etc/android.hardware.wifi.direct.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.wifi.direct.xml \
	frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
	frameworks/native/data/etc/android.hardware.usb.host.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.usb.host.xml \
	frameworks/native/data/etc/android.hardware.usb.accessory.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.usb.accessory.xml \
	frameworks/native/data/etc/android.hardware.camera.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.xml \
	frameworks/native/data/etc/android.hardware.camera.front.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.front.xml \

# WLAN configuration
# device specific wpa_supplicant.conf
PRODUCT_COPY_FILES += \
	device/samsung/erd9815/wpa_supplicant.conf:vendor/etc/wifi/wpa_supplicant.conf \
        device/samsung/erd9815/wifi/p2p_supplicant.conf:vendor/etc/wifi/p2p_supplicant.conf

# Bluetooth libbt namespace
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/libbt

# Bluetooth configuration
PRODUCT_COPY_FILES += \
       frameworks/native/data/etc/android.hardware.bluetooth.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.bluetooth.xml \
       hardware/samsung_slsi/libbt/conf/bt_did.conf:$(TARGET_COPY_OUT_VENDOR)/etc/bluetooth/bt_did.conf \
       frameworks/native/data/etc/android.hardware.bluetooth_le.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.bluetooth_le.xml

# Bluetooth Audio AIDL HAL(system)
PRODUCT_PACKAGES += \
    android.hardware.bluetooth.audio-impl

PRODUCT_PROPERTY_OVERRIDES += \
        wifi.interface=wlan0

# Packages needed for WLAN
# Note android.hardware.wifi@1.0-service is used by HAL interface 1.2
PRODUCT_PACKAGES += \
    android.hardware.wifi@1.0-service \
    android.hardware.wifi.supplicant@1.1-service \
    android.hardware.wifi.offload@1.0-service \
    dhcpcd.conf \
    hostapd \
    wificond \
    wifilog \
    wpa_supplicant.conf \
    wpa_supplicant \
    wpa_cli \
    hostapd_cli

PRODUCT_PACKAGES += \
    libwifi-hal \
    libwifi-system \
    libwifikeystorehal \
    android.hardware.wifi.supplicant@1.4 \
    android.hardware.wifi@1.5

# Bluetooth HAL
PRODUCT_PACKAGES += \
    android.hardware.bluetooth@1.0-service \
    android.hardware.bluetooth@1.0-impl \
    libbt-vendor

PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
    ro.bt.bdaddr_path=/sys/module/scsc_bt/parameters/bluetooth_address_fallback

# FEATURE_OPENGLES_EXTENSION_PACK support string config file
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.opengles.aep.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.opengles.aep.xml


PRODUCT_PROPERTY_OVERRIDES += \
	ro.opengles.version=196610 \
	ro.sf.lcd_density=480 \
	debug.slsi_platform=1 \
	debug.hwc.winupdate=1

# hw composer HAL
PRODUCT_PACKAGES += \
    vndservicemanager \
	hwcomposer.$(TARGET_BOOTLOADER_BOARD_NAME) \
    android.hardware.graphics.composer@2.4-impl \
    android.hardware.graphics.composer@2.4-service \
    vendor.samsung_slsi.hardware.ExynosHWCServiceTW@1.0-service

PRODUCT_PROPERTY_OVERRIDES += \
	debug.sf.disable_backpressure=1 \
	ro.surface_flinger.vsync_event_phase_offset_ns=0 \
	ro.surface_flinger.vsync_sf_event_phase_offset_ns=0 \
	ro.surface_flinger.max_frame_buffer_acquired_buffers=3 \
	ro.surface_flinger.running_without_sync_framework=false \
	ro.surface_flinger.use_color_management=0 \
	ro.surface_flinger.has_wide_color_display=1 \
	debug.sf.latch_unsignaled=1 \
	debug.sf.high_fps_late_app_phase_offset_ns=0 \
	debug.sf.high_fps_late_sf_phase_offset_ns=0

# set the dss enable status setup
ifeq ($(BOARD_USES_EXYNOS5_DSS_FEATURE), true)
PRODUCT_PROPERTY_OVERRIDES += \
        ro.exynos.dss=1
endif

# set the dss enable status setup
ifeq ($(BOARD_USES_EXYNOS_AFBC_FEATURE), true)
PRODUCT_PROPERTY_OVERRIDES += \
        ro.vendor.ddk.set.afbc=1
endif

# Set default USB interface
#PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
	persist.sys.usb.config=mtp,adb

PRODUCT_CHARACTERISTICS := phone

PRODUCT_AAPT_CONFIG := normal hdpi xhdpi xxhdpi
PRODUCT_AAPT_PREF_CONFIG := xxhdpi

# vpl files
PRODUCT_COPY_FILES += \
    device/samsung/erd9815/firmware/camera/libvpl/default_configuration.flm.cfg.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/default_configuration.flm.cfg.bin \
    device/samsung/erd9815/firmware/camera/libvpl/NFDV3_p4_7_qvga.nnc:$(TARGET_COPY_OUT_VENDOR)/firmware/NFDV3_p4_7_qvga.nnc

# swlme files
PRODUCT_COPY_FILES += \
    device/samsung/erd9815/firmware/camera/libswlme/lme408x229.cgo:$(TARGET_COPY_OUT_VENDOR)/firmware/lme408x229.cgo \
    device/samsung/erd9815/firmware/camera/libswlme/lme408x306.cgo:$(TARGET_COPY_OUT_VENDOR)/firmware/lme408x306.cgo \
    device/samsung/erd9815/firmware/camera/libswlme/lme500x282.cgo:$(TARGET_COPY_OUT_VENDOR)/firmware/lme500x282.cgo \
    device/samsung/erd9815/firmware/camera/libswlme/lme500x375.cgo:$(TARGET_COPY_OUT_VENDOR)/firmware/lme500x375.cgo \
    device/samsung/erd9815/firmware/camera/libswlme/lme520x292.cgo:$(TARGET_COPY_OUT_VENDOR)/firmware/lme520x292.cgo \
    device/samsung/erd9815/firmware/camera/libswlme/lme520x390.cgo:$(TARGET_COPY_OUT_VENDOR)/firmware/lme520x390.cgo

####################################
## VIDEO
####################################
# MFC firmware
PRODUCT_COPY_FILES += \
    device/samsung/erd9815/firmware/mfc_fw_v15.0.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/mfc_fw.bin

# 1. Codec 2.0
ifeq ($(BOARD_USE_DEFAULT_SERVICE), true)
# dummy service
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos/c2service

#DEVICE_MANIFEST_FILE += \
    device/samsung/erd9815/manifest_media_c2_default.xml

PRODUCT_COPY_FILES += \
    device/samsung/erd9815/media_codecs_performance_c2.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_performance_c2.xml

PRODUCT_PACKAGES += \
    samsung.hardware.media.c2@1.1-default-service
else
# exynos service
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/codec2

#DEVICE_MANIFEST_FILE += \
    device/samsung/erd9815/manifest_media_c2.xml

PRODUCT_COPY_FILES += \
    device/samsung/erd9815/media_codecs_c2.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_c2.xml \
    device/samsung/erd9815/media_codecs_performance_c2.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_performance_c2.xml

PRODUCT_PACKAGES += \
    samsung.hardware.media.c2@1.0-service \
    codec2.vendor.base.policy \
    codec2.vendor.ext.policy \
    libExynosC2ComponentStore \
    libExynosC2H264Dec \
    libExynosC2H264Enc \
    libExynosC2HevcDec \
    libExynosC2HevcEnc \
    libExynosC2Mpeg4Dec \
    libExynosC2Mpeg4Enc \
    libExynosC2H263Dec \
    libExynosC2H263Enc \
    libExynosC2Vp8Dec \
    libExynosC2Vp8Enc \
    libExynosC2Vp9Dec \
    libExynosC2Vp9Enc

PRODUCT_PROPERTY_OVERRIDES += \
    debug.stagefright.c2-poolmask=786432
endif

# 2. OpenMAX IL
PRODUCT_COPY_FILES += \
    frameworks/av/media/libstagefright/data/media_codecs_google_audio.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_google_audio.xml \
    frameworks/av/media/libstagefright/data/media_codecs_google_video.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_google_video.xml \
    device/samsung/erd9815/media_codecs_performance.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_performance.xml \
    device/samsung/erd9815/media_codecs.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs.xml

PRODUCT_COPY_FILES += \
    device/samsung/erd9815/seccomp_policy/mediacodec-seccomp.policy:$(TARGET_COPY_OUT_VENDOR)/etc/seccomp_policy/mediacodec.policy

PRODUCT_PACKAGES += \
    libstagefrighthw \
    libExynosOMX_Core \
    libExynosOMX_Resourcemanager \
    libOMX.Exynos.MPEG4.Decoder \
    libOMX.Exynos.AVC.Decoder \
    libOMX.Exynos.WMV.Decoder \
    libOMX.Exynos.VP8.Decoder \
    libOMX.Exynos.HEVC.Decoder \
    libOMX.Exynos.VP9.Decoder \
    libOMX.Exynos.MPEG4.Encoder \
    libOMX.Exynos.AVC.Encoder \
    libOMX.Exynos.VP8.Encoder \
    libOMX.Exynos.HEVC.Encoder \
    libOMX.Exynos.VP9.Encoder

# AVIExtractor
PRODUCT_PACKAGES += \
    libaviextractor
####################################

# 3. FilmGrainNoise
PRODUCT_PACKAGES += \
    libFilmGrainNoise \
    clFGN10BitNV12_32.bin \
    clFGN10BitNV12_64.bin \
    clFGN10BitYV12_32.bin \
    clFGN10BitYV12_64.bin \
    clFGN8BitNV12_32.bin \
    clFGN8BitNV12_64.bin \
    clFGN8BitYV12_32.bin \
    clFGN8BitYV12_64.bin

# Camera
PRODUCT_COPY_FILES += \
	device/samsung/erd9815/media_profiles.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_profiles_V1_0.xml

# Telephony
#PRODUCT_COPY_FILES += \
	frameworks/av/media/libstagefright/data/media_codecs_google_telephony.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_google_telephony.xml

#$(warning #### [WIFI] WLAN_VENDOR = $(WLAN_VENDOR))
#$(warning #### [WIFI] WLAN_CHIP = $(WLAN_CHIP))
#$(warning #### [WIFI] WLAN_CHIP_TYPE = $(WLAN_CHIP_TYPE))
#$(warning #### [WIFI] WIFI_NEED_CID = $(WIFI_NEED_CID))
#$(warning #### [WIFI] ARGET_BOARD_PLATFORM = $(ARGET_BOARD_PLATFORM))
#$(warning #### [WIFI] TARGET_BOOTLOADER_BOARD_NAME = $(TARGET_BOOTLOADER_BOARD_NAME))

#PRODUCT_COPY_FILES += device/samsung/erd9815/wpa_supplicant.conf:$(TARGET_COPY_OUT_VENDOR)/etc/wifi/wpa_supplicant.conf

# EPX firmware overwrite
#PRODUCT_COPY_FILES += \
	device/samsung/erd9815/firmware/epx.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/epx.bin

# Override heap growth limit due to high display density on device
# because this device has xxdpi screen.
# Note that this should be specified before reading XXX-dalvik-heap.mk
PRODUCT_PROPERTY_OVERRIDES +=           \
        dalvik.vm.heapgrowthlimit=256m  \
        dalvik.vm.heapminfree=2m

# setup dalvik vm configs.
$(call inherit-product, frameworks/native/build/phone-xhdpi-2048-dalvik-heap.mk)

PRODUCT_TAGS += dalvik.gc.type-precise

#GPS
PRODUCT_PACKAGES += \
   android.hardware.gnss@2.1-impl \
   vendor.samsung.hardware.gnss@1.0-impl \
   vendor.samsung.hardware.gnss@1.0-service

ifeq ($(BOARD_USES_EXYNOS_GNSS_HAL), true)
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.location.gps.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.location.gps.xml
endif

# Exynos OpenVX framework
#PRODUCT_PACKAGES += \
		libexynosvision

#ifeq ($(TARGET_USES_CL_KERNEL),true)
#PRODUCT_PACKAGES += \
       libopenvx-opencl
#endif

#PRODUCT_PROPERTY_OVERRIDES += \
        persist.vendor.gnsslog.maxfilesize=256 \
        persist.vendor.gnsslog.status=0 \
        vendor.exynos.gnsslog=/data/vendor/gps/

# Copy SCore FW
# SCore develop1 firmware
#ifneq ($(filter full_erd9815_evt0 full_erd9815_evt0_s5100,$(TARGET_PRODUCT)),)
#PRODUCT_COPY_FILES += \
	device/samsung/erd9815/firmware/score/evt0/develop1/score_ts_dmb.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/score/develop1/score_ts_dmb.bin \
	device/samsung/erd9815/firmware/score/evt0/develop1/score_ts_pmw.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/score/develop1/score_ts_pmw.bin \
	device/samsung/erd9815/firmware/score/evt0/develop1/score_br_dmb.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/score/develop1/score_br_dmb.bin \
	device/samsung/erd9815/firmware/score/evt0/develop1/score_br_pmw.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/score/develop1/score_br_pmw.bin
#else
#PRODUCT_COPY_FILES += \
	device/samsung/erd9815/firmware/score/evt1/develop1/score_ts_dmb.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/score/develop1/score_ts_dmb.bin \
	device/samsung/erd9815/firmware/score/evt1/develop1/score_ts_pmw.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/score/develop1/score_ts_pmw.bin \
	device/samsung/erd9815/firmware/score/evt1/develop1/score_br_dmb.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/score/develop1/score_br_dmb.bin \
	device/samsung/erd9815/firmware/score/evt1/develop1/score_br_pmw.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/score/develop1/score_br_pmw.bin
#endif
# SCore camera 1 firmware
# PRODUCT_COPY_FILES += \
	device/samsung/erd9815/firmware/score/score_dummy:$(TARGET_COPY_OUT_VENDOR)/firmware/score/camera1/score_dummy
# SCore camera2 firmware
# PRODUCT_COPY_FILES += \
	device/samsung/erd9815/firmware/score/score_dummy:$(TARGET_COPY_OUT_VENDOR)/firmware/score/camera2/score_dummy
# SCore camera3 firmware
# PRODUCT_COPY_FILES += \
	device/samsung/erd9815/firmware/score/score_dummy:$(TARGET_COPY_OUT_VENDOR)/firmware/score/camera3/score_dummy

#ifeq ($(BOARD_USES_OPENVX),true)
# IVA firmware
#PRODUCT_COPY_FILES += \
        device/samsung/erd9815/firmware/iva30_rt-makalu.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/iva30_rt-makalu.bin
#endif

#Gatekeeper
PRODUCT_PACKAGES += \
	android.hardware.gatekeeper@1.0-impl \
	android.hardware.gatekeeper@1.0-service \
	gatekeeper.$(TARGET_SOC)

#CryptoManager
PRODUCT_PACKAGES += \
	tlcmdrv

# CryptoManager test app for eng build
ifneq (,$(filter eng, $(TARGET_BUILD_VARIANT)))
	PRODUCT_PACKAGES += \
	tlcmtest \
	cm_test
endif

PRODUCT_PACKAGES += \
	exynos-thermald

# Epic daemon
PRODUCT_SOONG_NAMESPACES += device/samsung/erd9815/power
PRODUCT_PACKAGES += epic libems_service libexynos_migov libepic_helper

PRODUCT_COPY_FILES += \
        device/samsung/erd9815/power/epic.json:$(TARGET_COPY_OUT_VENDOR)/etc/epic.json \
        device/samsung/erd9815/power/ems.json:$(TARGET_COPY_OUT_VENDOR)/etc/ems.json

# Epic HIDL
SOONG_CONFIG_NAMESPACES += epic
SOONG_CONFIG_epic := vendor_hint
SOONG_CONFIG_epic_vendor_hint := true

PRODUCT_PACKAGES += \
	vendor.samsung_slsi.hardware.epic@1.0-impl \
	vendor.samsung_slsi.hardware.epic@1.0-service

# ipsec_tunnels feature
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.software.ipsec_tunnels.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.software.ipsec_tunnels.xml

# TUI
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/$(TARGET_SOC_BASE)/secapp/FFFF0000000000000000000000000014.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/ffff0000000000000000000000000014.tlbin \
	vendor/samsung_slsi/$(TARGET_SOC_BASE)/secapp/070C0000000000000000000000000000.tlbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/070c0000000000000000000000000000.tlbin

# Eden
PRODUCT_PACKAGES += \
        android.hardware.neuralnetworks@1.3-service.eden-drv \
        vendor.samsung_slsi.hardware.eden_runtime@1.0-impl \
        vendor.samsung_slsi.hardware.eden_runtime@1.0-service \
        vendor.samsung_slsi.hardware.eden_runtime@1.0 \
        libeden_rt_stub.edensdk.samsung \
        libeden_nn_on_system \
        public.libraries-edensdk.samsung.txt

PRODUCT_PROPERTY_OVERRIDES += \
	log.tag.EDEN=INFO \
	ro.vendor.eden.devices=CPU1_GPU1_NPU1_DSP1 \
	ro.vendor.eden.npu.version.path=/sys/devices/platform/npu_exynos/version \
	ro.vendor.eden.core_mask=112 \
	ro.vendor.eden.mrs=32

PRODUCT_COPY_FILES += \
	device/samsung/erd9815/eden/eden_kernel_64.bin:$(TARGET_COPY_OUT_VENDOR)/etc/eden/gpu/eden_kernel_64.bin \
	device/samsung/erd9815/firmware/NPU.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/NPU.bin \
	device/samsung/erd9815/eden/enn_preset.json:$(TARGET_COPY_OUT_VENDOR)/etc/eden/enn_preset.json

# OFI
PRODUCT_PACKAGES += \
	vendor.samsung_slsi.hardware.ofi@2.1-service

PRODUCT_SOONG_NAMESPACES += vendor/samsung_slsi/hardware/ofi/2.1/default_E9815 \
	vendor/samsung_slsi/exynos/ofi/2.1_E9815
PRODUCT_SOONG_NAMESPACES += vendor/samsung_slsi/exynos/enn_driver

PRODUCT_COPY_FILES += \
  device/samsung/erd9815/ofi/ofi_preset.json:$(TARGET_COPY_OUT_VENDOR)/etc/ofi/ofi_preset.json \
  device/samsung/erd9815/firmware/dsp/dsp.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp.bin \
  device/samsung/erd9815/firmware/dsp/dsp_iac_dm.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_iac_dm.bin \
  device/samsung/erd9815/firmware/dsp/dsp_iac_pm.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_iac_pm.bin \
  device/samsung/erd9815/firmware/dsp/dsp_ivp_dm.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_ivp_dm.bin \
  device/samsung/erd9815/firmware/dsp/dsp_ivp_pm.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_ivp_pm.bin \
  device/samsung/erd9815/firmware/dsp/dsp_master.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_master.bin \
  device/samsung/erd9815/firmware/dsp/dsp_sec.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_sec.bin \
  device/samsung/erd9815/firmware/dsp/dsp_iac_dm_sec.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_iac_dm_sec.bin \
  device/samsung/erd9815/firmware/dsp/dsp_iac_pm_sec.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_iac_pm_sec.bin \
  device/samsung/erd9815/firmware/dsp/dsp_ivp_dm_sec.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_ivp_dm_sec.bin \
  device/samsung/erd9815/firmware/dsp/dsp_ivp_pm_sec.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_ivp_pm_sec.bin \
  device/samsung/erd9815/firmware/dsp/dsp_master_sec.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_master_sec.bin \
  vendor/samsung_slsi/s5e9815/secapp/ffffffff01b85acba7861897fc90973d.tabin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/ffffffff01b85acba7861897fc90973d.tabin \
  vendor/samsung_slsi/s5e9815/secapp/ffffffff000000000000000000007777.drbin:$(TARGET_COPY_OUT_VENDOR)/app/mcRegistry/ffffffff000000000000000000007777.drbin \
  device/samsung/erd9815/firmware/dsp/dsp_reloc_rules.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_reloc_rules.bin \
  device/samsung/erd9815/firmware/dsp/libivp.elf:$(TARGET_COPY_OUT_VENDOR)/firmware/libivp.elf \
  device/samsung/erd9815/firmware/dsp/liblog.elf:$(TARGET_COPY_OUT_VENDOR)/firmware/liblog.elf \
  device/samsung/erd9815/firmware/dsp/libnn.elf:$(TARGET_COPY_OUT_VENDOR)/firmware/libnn.elf \
  device/samsung/erd9815/firmware/dsp/dsp_gkt.xml:$(TARGET_COPY_OUT_VENDOR)/firmware/dsp_gkt.xml \
  device/samsung/erd9815/firmware/dsp/liblme.elf:$(TARGET_COPY_OUT_VENDOR)/firmware/liblme.elf

PRODUCT_PACKAGES += \
    vendor.samsung_slsi.hardware.SbwcDecompService@1.0-service

PRODUCT_PACKAGES += \
	android.hardware.renderscript@1.0-impl

PRODUCT_PROPERTY_OVERRIDES += \
	ro.frp.pst=/dev/block/platform/13100000.ufs/by-name/frp

# WFD SINK buffer plane info
# this property should be same value with BOARD_USE_SINGLE_PLANE_IN_DRM
PRODUCT_PROPERTY_OVERRIDES += \
	ro.hw.wfd_use_single_plane_in_drm=0

# RenderScript HAL
PRODUCT_PACKAGES += \
	android.hardware.renderscript@1.0-impl

#VNDK
PRODUCT_PACKAGES += \
	vndk-libs

PRODUCT_ENFORCE_RRO_TARGETS := \
	framework-res

# Keymaster
PRODUCT_PACKAGES += \
	android.hardware.keymaster@4.0-impl \
	android.hardware.keymaster@4.0_tee-service \
	tlkeymasterM

PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos/tee/TlcTeeKeymaster4

ifneq ($(filter eng userdebug,$(TARGET_BUILD_VARIANT)),)
PRODUCT_PACKAGES += \
    km_keybox_attestation
endif

ifeq ($(BOARD_USES_KEYMASTER_VER1),true)
PRODUCT_PACKAGES += \
	keystore.$(TARGET_SOC) \
	tlkeymasterM
endif

# MALI or SWIFTSHADER
ifeq ($(USE_SWIFTSHADER),true)
PRODUCT_PACKAGES += \
	libEGL_swiftshader \
	libGLESv1_CM_swiftshader \
	libGLESv2_swiftshader

PRODUCT_PROPERTY_OVERRIDES += \
       ro.hardware.egl = swiftshader
else
ifneq ($(MALI_BUILD),true)

ifeq ($(ENABLE_T_FEATURE),true)
MALI_VERSION ?= r38p1
else
MALI_VERSION ?= r26p0
endif

ifeq ($(BOARD_USES_EXYNOS_GRALLOC_VERSION),4)
PRODUCT_SOONG_NAMESPACES += vendor/samsung_slsi/s5e9815/libs/${MALI_VERSION}
else
PRODUCT_SOONG_NAMESPACES += vendor/samsung_slsi/s5e9815/libs/${MALI_VERSION}/legacy_gralloc
endif

ifeq ($(ENABLE_T_FEATURE),true)
PRODUCT_PACKAGES += \
         libGLES_mali \
         libOpenCL_prebuilt \
         libOpenCL_symlink \
         vulkan.mali_prebuilt \
         libgpudataproducer \
         whitelist
# Renderscript libs
PRODUCT_SOONG_NAMESPACES += vendor/samsung_slsi/s5e9815/libs/${MALI_VERSION}/renderscript
PRODUCT_PACKAGES += \
         libbccArm \
         libbcc_mali \
         libLLVM_android_mali \
         libRSDriverArm \
         bcc_mali
PRODUCT_COPY_FILES += \
         vendor/samsung_slsi/s5e9815/libs/$(MALI_VERSION)/renderscript/lib64/libclcore.bc:$(TARGET_COPY_OUT_VENDOR)/lib64/libclcore.bc \
         vendor/samsung_slsi/s5e9815/libs/$(MALI_VERSION)/renderscript/lib64/libmalicore.bc:$(TARGET_COPY_OUT_VENDOR)/lib64/libmalicore.bc \
         vendor/samsung_slsi/s5e9815/libs/$(MALI_VERSION)/renderscript/lib/libclcore.bc:$(TARGET_COPY_OUT_VENDOR)/lib/libclcore.bc \
         vendor/samsung_slsi/s5e9815/libs/$(MALI_VERSION)/renderscript/lib/libmalicore.bc:$(TARGET_COPY_OUT_VENDOR)/lib/libmalicore.bc \
         vendor/samsung_slsi/s5e9815/libs/$(MALI_VERSION)/renderscript/lib/libclcore_neon.bc:$(TARGET_COPY_OUT_VENDOR)/lib/libclcore_neon.bc
else
PRODUCT_PACKAGES += \
	libGLES_mali \
	libOpenCL \
	vulkan.mali \
	vulkan.erd9815_r \
	libbccArm_valhall \
	libbccArm_valhall_nt \
	libLLVM_android_mali \
	libbcc_mali \
	libRSDriverArm \
	bcc_mali \
	whitelist
PRODUCT_COPY_FILES += \
	vendor/samsung_slsi/s5e9815/libs/$(MALI_VERSION)/lib/libclcore.bc:$(TARGET_COPY_OUT_VENDOR)/lib/libclcore.bc \
	vendor/samsung_slsi/s5e9815/libs/$(MALI_VERSION)/lib/libclcore_neon.bc:$(TARGET_COPY_OUT_VENDOR)/lib/libclcore_neon.bc \
	vendor/samsung_slsi/s5e9815/libs/$(MALI_VERSION)/lib/libmalicore.bc:$(TARGET_COPY_OUT_VENDOR)/lib/libmalicore.bc \
	vendor/samsung_slsi/s5e9815/libs/$(MALI_VERSION)/lib64/libclcore.bc:$(TARGET_COPY_OUT_VENDOR)/lib64/libclcore.bc \
	vendor/samsung_slsi/s5e9815/libs/$(MALI_VERSION)/lib64/libmalicore.bc:$(TARGET_COPY_OUT_VENDOR)/lib64/libmalicore.bc
endif
endif

ifeq ($(ENABLE_T_FEATURE),true)
PRODUCT_PROPERTY_OVERRIDES += \
	ro.hardware.vulkan=mali
# control oem unlock property
PRODUCT_PROPERTY_OVERRIDES += ro.oem_unlock_supported=1
endif
PRODUCT_PROPERTY_OVERRIDES += \
	ro.hardware.egl = mali

ifeq ($(ENABLE_T_FEATURE),true)
#include vendor/arm/mali_src/android/renderscript.device.mk
SOONG_CONFIG_NAMESPACES += mali_rs
SOONG_CONFIG_mali_rs += \
			    gpu_arch

SOONG_CONFIG_mali_rs_gpu_arch := VALHALL
endif
endif

# vulkan version information
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.vulkan.compute-0.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.compute.xml \
	frameworks/native/data/etc/android.hardware.vulkan.level-1.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.level.xml

ifeq ($(ENABLE_T_FEATURE),true)
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.vulkan.version-1_3.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.version.xml \
	frameworks/native/data/etc/android.software.vulkan.deqp.level-2022-03-01.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.software.vulkan.deqp.level.xml
else
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.vulkan.version-1_1.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.version.xml \
	frameworks/native/data/etc/android.software.vulkan.deqp.level-2020-03-01.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.software.vulkan.deqp.level.xml
endif

#vendor directory packages
#+PRODUCT_PACKAGES += \
	libbccArm_bifrost \
	libLLVM_android_mali \
	libOpenCL \
	libOpenCL32 \
	libclcore \
	libclcore32 \
	libclcore_neon \
	libbcc_mali \
	bcc_mali \
	whitelist \
	libstagefright_hdcp \
	libskia_opt

PRODUCT_PACKAGES += \
	mfc_fw.bin \
	calliope_sram.bin \
	calliope_dram.bin \
	vts.bin \
	dsm.bin \
	APBargeIn_AUDIO_SLSI.bin \
	AP_AUDIO_SLSI.bin \
	APBiBF_AUDIO_SLSI.bin \
	APDV_AUDIO_SLSI.bin \
	NPU.bin

#FOR userspace
$(call inherit-product, $(SRC_TARGET_DIR)/product/emulated_storage.mk)

# NFC Feature
$(call inherit-product-if-exists, device/samsung/erd9815/nfc/device-nfc.mk)

PRODUCT_COPY_FILES += \
	device/samsung/erd9815/exynos-thermal/exynos-thermal.conf:$(TARGET_COPY_OUT_VENDOR)/exynos-thermal.conf \
	device/samsung/erd9815/exynos-thermal/exynos-thermal.env:$(TARGET_COPY_OUT_VENDOR)/exynos-thermal.env

$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/aosp_base.mk)
$(call inherit-product, hardware/samsung_slsi/exynos5/exynos5.mk)
ifeq ($(PRODUCT_USE_VIRTUAL_AB),true)
$(call inherit-product, $(SRC_TARGET_DIR)/product/virtual_ab_ota.mk)
endif
$(call inherit-product-if-exists, vendor/samsung_slsi/telephony/common/device-vendor.mk)
$(call inherit-product-if-exists, hardware/samsung_slsi/s5e9815/s5e9815.mk)
$(call inherit-product-if-exists, vendor/samsung_slsi/common/exynos-vendor.mk)
#$(call inherit-product-if-exists, vendor/samsung_slsi/exynos/eden/eden.mk)
$(call inherit-product-if-exists, vendor/samsung_slsi/exynos/ofi/2.1_E9815/ofi.mk)
$(call inherit-product-if-exists, vendor/samsung_slsi/exynos/camera/O21/ww/hal3/src/camera.mk)

## IMSService ##
# IMSService build for both types (source build, prebulit apk/so build).
# Please make sure that only one of both ShannonIms(apk git) shannon-ims(src git) is present in the repo.
# This will be called only if IMSService is building with prebuilt binary for stable branches.
$(call inherit-product-if-exists, packages/apps/ShannonIms/device-vendor.mk)
$(call inherit-product-if-exists, packages/apps/ShannonIms/device-vendor-testauto.mk)
# This will be called only if IMSService is building with source code for dev branches.
$(call inherit-product-if-exists, vendor/samsung_slsi/ims/shannon-ims/device-vendor.mk)
$(call inherit-product-if-exists, vendor/samsung_slsi/ims/shannon-qns/device-vendor.mk)
$(call inherit-product-if-exists, vendor/samsung_slsi/ims/shannon-iwlan/device-vendor.mk)
$(call inherit-product-if-exists, vendor/samsung_slsi/ims/packetrouter/device-vendor.mk)
$(call inherit-product-if-exists, external/strongswan/device-vendor.mk)

## WLBT Logging ##
PRODUCT_PROPERTY_OVERRIDES += \
        persist.vendor.wlbtlog.maxfilesize=50 \
        persist.vendor.wlbtlog.maxfiles=5

$(call inherit-product, device/samsung/erd9815/scsc_wlbt.mk)
$(call inherit-product, vendor/samsung_slsi/scsc_tools/wlbt/device-vendor.mk)

#supplicant configs
ifneq ($(BOARD_WPA_SUPPLICANT_DRIVER),)
CONFIG_DRIVER_$(BOARD_WPA_SUPPLICANT_DRIVER) := y
endif
WPA_SUPPL_DIR = external/wpa_supplicant_8
include $(WPA_SUPPL_DIR)/wpa_supplicant/wpa_supplicant_conf.mk
include $(WPA_SUPPL_DIR)/wpa_supplicant/android.config

# for off charging mode
PRODUCT_PACKAGES += \
	charger_res_images

$(call inherit-product, device/samsung/erd9815/gnss_binaries/gnss_binaries.mk)

#Enable protected contents on SurfaceFlinger
PRODUCT_PROPERTY_OVERRIDES += \
	ro.surface_flinger.protected_contents=1

## DumpDaemon ##
ifneq ($(filter eng userdebug,$(TARGET_BUILD_VARIANT)),)
PRODUCT_PACKAGES += QuickdumpD
endif

# GMS Applications
ifeq ($(WITH_GMS),true)
GMS_ENABLE_OPTIONAL_MODULES := true
USE_GMS_STANDARD_CONFIG := true
$(call inherit-product-if-exists, vendor/partner_gms/products/gms.mk)
endif

# hw composer property : Properties related to hwc will be defined in hwcomposer_property.mk
$(call inherit-product-if-exists, hardware/samsung_slsi/graphics/base/hwcomposer_property.mk)
