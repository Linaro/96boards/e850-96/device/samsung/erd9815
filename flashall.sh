adb reboot bootloader

fastboot flash bootloader out/target/product/erd9815/bootloader.img
fastboot flash dtbo out/target/product/erd9815/dtbo.img
fastboot flash boot out/target/product/erd9815/boot.img
fastboot flash vendor_boot out/target/product/erd9815/vendor_boot.img
fastboot flash vbmeta out/target/product/erd9815/vbmeta.img
fastboot flash vbmeta_system out/target/product/erd9815/vbmeta_system.img
fastboot flash recovery out/target/product/erd9815/recovery.img

fastboot reboot fastboot

fastboot flash vendor out/target/product/erd9815/vendor.img
fastboot flash system out/target/product/erd9815/system.img
fastboot reboot -w
